//
//  ExtensionClass.swift
//  DSM
//
//  Created by Rajneesh Kumar on 23/03/18.
//  Copyright © 2018 Rajneesh Kumar. All rights reserved.
//

import UIKit


@IBDesignable class ExtButton: UIButton
{
    
    @IBInspectable var shadowColor:UIColor = UIColor.clear{
        didSet{
            
            self.layer.shadowColor = shadowColor.cgColor
            
        }
    }
    
    @IBInspectable var shadowOpacity : CGFloat = 0{
        
        didSet{
            
            self.layer.shadowOpacity = Float(shadowOpacity)
            
        }
    }
    
    @IBInspectable var shadowOffset : CGSize = CGSize(width: -0, height: 0){
        
        didSet{
            
            self.layer.shadowOffset =  shadowOffset
            
        }
    }
    
    @IBInspectable var shadowRadius : CGFloat = 0.0
        {
        didSet{
            
            self .layer .shadowRadius = CGFloat(shadowRadius)
            
        }
    }
    
    
    @IBInspectable var cornerRadius:CGFloat = 0
        {
        didSet{
            
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderWidth:CGFloat = 0
        {
        
        didSet{
            
            self.layer.borderWidth = borderWidth
            
        }
    }
    
    @IBInspectable var borderColor:UIColor = UIColor.clear{
        
        didSet{
            self.layer.borderColor = borderColor.cgColor
        }
        
    }
}

//MARK:- For TextField

@IBDesignable class textFieldExt: UITextField
{
    
    @IBInspectable var shadowColor:UIColor = UIColor.clear{
        
        didSet{
            
            self.layer.shadowColor = shadowColor.cgColor
            
        }
    }
    
    @IBInspectable var shadowOpacity : CGFloat = 0.0
        {
        
        didSet{
            
            self.layer.shadowOpacity = Float(shadowOpacity)
            
        }
    }
    
    @IBInspectable var shadowOffset : CGSize = CGSize(width: -0, height: 0){
        
        didSet{
            
            self.layer.shadowOffset =  shadowOffset
            
        }
    }
    
    @IBInspectable var shadowRadius : CGFloat = 0.0
        {
        didSet{
            
            self .layer .shadowRadius = CGFloat(shadowRadius)
            
        }
    }
    
    @IBInspectable var leftImage: UIImage?
        {
        didSet
        {
            updateView()
        }
    }
    
    @IBInspectable var rightImage: UIImage?
        {
        didSet
        {
            updateView()
        }
    }
    
    
    
    @IBInspectable var leftPadding: CGFloat = 0
        {
        didSet
        {
            updateView()
        }
    }
    
    
    @IBInspectable var rightPadding: CGFloat = 0
        {
        didSet
        {
            applyPaddingonTextField()
        }
    }
    
    
    @IBInspectable var cornerRadius:CGFloat = 0
        {
        didSet
        {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderWidth:CGFloat = 0
        {
        didSet
        {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor:UIColor = UIColor.clear
        {
        didSet
        {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var leadingImage: UIImage?
        {
        didSet
        {
            updateView()
        }
    }
    
    
    func applyPaddingonTextField()
    {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
        //
        //        self.isEnabled = true
        //        self.borderStyle = UITextBorderStyle.none
        //        self.layer.borderWidth = 1
        //        self.layer.borderColor = UIColor.lightGray.cgColor
        //        self.layer.masksToBounds = true
        
    }
    
    func updateView()
    {
        if let image = leftImage
        {
            leftViewMode = .always
            let imageView = UIImageView(frame: CGRect(x: leftPadding, y: 0, width: 20, height: 20))
            imageView.image = image
            imageView.tintColor = tintColor
            var width = leftPadding + 20
            
            if borderStyle == UITextBorderStyle.none || borderStyle == UITextBorderStyle.line
            {
                width = width + 5
                
            }
            let view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 20))
            view.addSubview(imageView)
            leftView = view
            
            //This is For Two Sided Corner Radius
            view.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        }
        else
        {
            //image is nill
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: leftPadding, height: self.frame.size.height))
            self.leftView = paddingView
            leftViewMode = .always
            
        }
        // Placeholder text Color
        
    }
}


//  uiLabel class.

@IBDesignable class ExtLabel: UILabel
{
    @IBInspectable var cornerRadius:CGFloat = 0
        {
        didSet{
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderWidth:CGFloat = 0.0
        {
        didSet{
            self.layer.borderWidth = borderWidth
        }
    }
    
    
    
    
    @IBInspectable var borderColor:UIColor = UIColor.clear
        {
        didSet
        {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    
    
    @IBInspectable var clipsBounds: Bool = true
        {
        didSet
        {
            self.clipsToBounds = clipsBounds
        }
    }
    
    @IBInspectable var masksToBounds:Bool = true
        {
        
        didSet{
            
            self.layer.masksToBounds = masksToBounds
            
        }
    }
    
    @IBInspectable var shadow_Color:UIColor = UIColor.clear{
        
        didSet{
            
            self.layer.shadowColor = shadow_Color.cgColor
            
        }
    }
    
    @IBInspectable var shadowOpacity : CGFloat = 0.0
        {
        
        didSet{
            
            self.layer.shadowOpacity = Float(shadowOpacity)
            
        }
    }
    
    @IBInspectable var shadow_Offset : CGSize = CGSize(width: -0, height: 0){
        
        didSet{
            
            self.layer.shadowOffset =  shadow_Offset
            
        }
    }
    
    @IBInspectable var shadowRadius : CGFloat = 0.0
        {
        didSet{
            
            self .layer .shadowRadius = CGFloat(shadowRadius)
            
        }
    }
}

@IBDesignable class ExtView: UIView
{
    @IBInspectable var cornerRadius:CGFloat = 0.0
        {
        didSet{
            
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    
    @IBInspectable var borderWidth:CGFloat = 0.0
        {
        didSet{
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor:UIColor = UIColor.clear{
        
        didSet{
            self.layer.borderColor = borderColor.cgColor
            
        }
    }
    
    @IBInspectable var masksToBounds:Bool = true
        {
        
        didSet{
            
            self.layer.masksToBounds = masksToBounds
            
        }
    }
    
    @IBInspectable var shadowColor:UIColor = UIColor.clear{
        
        didSet{
            
            self.layer.shadowColor = shadowColor.cgColor
            
        }
    }
    
    @IBInspectable var shadowOpacity : CGFloat = 0.0
        {
        
        didSet{
            
            self.layer.shadowOpacity = Float(shadowOpacity)
            
        }
    }
    
    @IBInspectable var shadowOffset : CGSize = CGSize(width: -0, height: 0){
        
        didSet{
            
            self.layer.shadowOffset =  shadowOffset
            
        }
    }
    
    @IBInspectable var shadowRadius : CGFloat = 0.0
        {
        didSet{
            
            self .layer .shadowRadius = CGFloat(shadowRadius)
            
        }
    }
    
    
    @IBInspectable var twosidedcornerRadius : CGFloat = 0.0
        {
        didSet{
            
            self.clipsToBounds = true
            self.layer.cornerRadius = twosidedcornerRadius
            self.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
            
            // self.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
            
        }
    }
    
    
    
}


@IBDesignable class ExtImg : UIImageView
{
    
    @IBInspectable var cornerRadiusforTwoSideOnly : CGFloat = 0.0
        {
        didSet{
            
            self.clipsToBounds = true
            self.layer.cornerRadius = cornerRadiusforTwoSideOnly
            
            if #available(iOS 11.0, *) {
                self.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
            } else {
                
            }
            
        }
    }
    
    @IBInspectable var cornerRadius:CGFloat = 0.0
        {
        didSet{
            
            self.layer.cornerRadius = cornerRadius
        }
    }
    @IBInspectable var clipsBounds: Bool = true
        {
        didSet
        {
            self.clipsToBounds = clipsBounds
        }
    }
    
    @IBInspectable var masksToBounds:Bool = true
        {
        
        didSet{
            
            self.layer.masksToBounds = masksToBounds
            
        }
    }
    
}
@IBDesignable class ExtViewText: UITextView
{
    @IBInspectable var cornerRadius:CGFloat = 0.0
        {
        didSet{
            
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    
    @IBInspectable var borderWidth:CGFloat = 0.0
        {
        didSet{
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor:UIColor = UIColor.clear{
        
        didSet{
            self.layer.borderColor = borderColor.cgColor
            
        }
    }
    
    @IBInspectable var masksToBounds:Bool = true
        {
        
        didSet{
            
            self.layer.masksToBounds = masksToBounds
            
        }
    }
    
    @IBInspectable var shadowColor:UIColor = UIColor.clear{
        
        didSet{
            
            self.layer.shadowColor = shadowColor.cgColor
            
        }
    }
    
    @IBInspectable var shadowOpacity : CGFloat = 0.0
        {
        
        didSet{
            
            self.layer.shadowOpacity = Float(shadowOpacity)
            
        }
    }
    
    @IBInspectable var shadowOffset : CGSize = CGSize(width: -0, height: 0){
        
        didSet{
            
            self.layer.shadowOffset =  shadowOffset
            
        }
    }
    
    @IBInspectable var shadowRadius : CGFloat = 0.0
        {
        didSet{
            
            self .layer .shadowRadius = CGFloat(shadowRadius)
            
        }
    }
    
    
    @IBInspectable var twosidedcornerRadius : CGFloat = 0.0
        {
        didSet{
            
            self.clipsToBounds = true
            self.layer.cornerRadius = twosidedcornerRadius
            self.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
            
            // self.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
            
        }
    }
    
    
    
}




