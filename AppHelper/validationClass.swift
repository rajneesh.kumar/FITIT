//
//  validationClass.swift
//  FITIT
//
//  Created by Rajneesh Kumar on 13/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit

class validationClass: NSObject {
    
    static let validationImstance = validationClass()
    
    
    
    func is_textFieldEmpty(txtFiled : UITextField) -> Bool{
        if (txtFiled.text?.count)! > 0{
            return false
        }else{
            return true
        }
    }
    
    func isPasswordMatch(password : UITextField , confirmPassword : UITextField) -> Bool{
        if password.text == confirmPassword.text{
            return true
        }
        else{
            return false
        }
    }
    
    func isValidEmail(email:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    
    
    
    
    
    
    
    
    
}
