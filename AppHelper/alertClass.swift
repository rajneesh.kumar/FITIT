//
//  alertClass.swift
//  FITIT
//
//  Created by Rajneesh Kumar on 07/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit

class alertClass: NSObject {
    
    static let alertInstance = alertClass()
    
    func showAlertWithMessage(Message : String , ControllerName : UIViewController){
        
        let alert = UIAlertController(title: "", message: Message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        ControllerName.present(alert, animated: true, completion: nil)
    }
}

