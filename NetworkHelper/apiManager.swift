//
//  apiHelper.swift
//  FITIT
//
//  Created by Rajneesh Kumar on 07/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//


import SwiftyJSON
import SVProgressHUD


enum httpmethod : String {
    case post = "POST"
    case get = "GET"
}


class apiManager: NSObject
{
    static let sharedInstance =  apiManager()
    
     func getPostString(params:[String:Any]) -> String
    {
        var data = [String]()
        for(key, value) in params
        {
            data.append(key + "=\(value)")
        }
        return data.map { String($0) }.joined(separator: "&")
    }
    
    
    
    func handleApiCall( controllerName : UIViewController , apiUrl : String , parameter : [String : Any] , requestType : httpmethod, onCompletion : @escaping (JSON) -> Void){
        
        self.showAlert(viewController: controllerName)
        let url = URL(string: apiUrl)
        
        print("api url \(apiUrl)")
        
        
        var request = URLRequest(url:url!)
        request.timeoutInterval = 30.0
        
        if requestType.rawValue == httpmethod.get.rawValue{
            request.httpMethod = httpmethod.get.rawValue
        }
        else{
            request.httpMethod = httpmethod.post.rawValue
           // request.encodeParameters(parameters: parameter)
             let postString = self.getPostString(params: parameter)
            request.httpBody = postString.data(using: .utf8)
        }
        
        //set the header ---
        
        let config = URLSessionConfiguration.default
        config.httpAdditionalHeaders = [
            "Accept" : "application/json",
            "Content-Type" : "application/x-www-form-urlencoded"
        ]
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if error == nil{
                do{
                    let responseJson = try JSON(data: data!)
                     print(responseJson)
                    onCompletion(responseJson)
                } catch{}
            }
            else{
                print("some occured")
            }
        }
        task.resume()
    }
    
    
    
    
    func showAlert( viewController : UIViewController) -> Void{
        
            if !networkManager.networkInstance.connectionAvailable(){
            
                SVProgressHUD .dismiss()
                
                let refreshAlert = UIAlertController(title: "", message: "Internet Connection Required ", preferredStyle: UIAlertControllerStyle.alert)
                
                let Cancel = UIAlertAction(title:"RETRY", style: .default, handler: {(UIAlertAction) -> Void in
                    
                    if !networkManager.networkInstance.connectionAvailable(){
                        self.showAlert(viewController: viewController)
                    }
                })
                refreshAlert.addAction(Cancel)
                viewController.present(refreshAlert, animated: true, completion: nil)
        }
     }
}











//extension URLRequest {
//    
//    private func percentEscapeString(_ string: String) -> String {
//        var characterSet = CharacterSet.alphanumerics
//        characterSet.insert(charactersIn: "-._* ")
//        
//        return string
//            .addingPercentEncoding(withAllowedCharacters: characterSet)!
//            .replacingOccurrences(of: " ", with: "+")
//            .replacingOccurrences(of: " ", with: "+", options: [], range: nil)
//    }
//    
//    mutating func encodeParameters(parameters: [String : Any]) {
//        
//        let parameterArray = parameters.map { (arg) -> String in
//            let (key, value) = arg
//            return "\(key)=\(self.percentEscapeString(value as! String))"
//        }
//        
//        httpBody = parameterArray.joined(separator: "&").data(using: String.Encoding.utf8)
//    }
//}

