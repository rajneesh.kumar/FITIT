//
//  apiUrl.swift
//  FITIT
//
//  Created by Rajneesh Kumar on 07/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//


//let baseUrl = "http://192.168.101.244/fitit/index.php/api/"
let baseUrl = "https://demo.invetech.in/fitit/api/"


                // for both trainee and trainning--
let loginUrlApi = "\(baseUrl)login"
let signupUrlApi = "\(baseUrl)signup_1"


                        //MARK: - ALL MASTER API --
let examptlistApi = "\(baseUrl)trainer/exempt_list"
let coordinateListApi = "\(baseUrl)trainer/coordinator_list"
let trainningTypeListApi = "\(baseUrl)trainer/training_type_list"
let locationListApi = "\(baseUrl)trainer/location_list"
let getWorkoutPositionlist = "\(baseUrl)trainer/workout_position_list"



                        //MARK: - TRAINEE API (Training)

let getIntrestListApi = "\(baseUrl)trainee/interest/list"
let getGoalListApi = "\(baseUrl)trainee/goals/list"

let traineeSignUpApi = "\(baseUrl)trainee/signup_2"
let trineeSignUP_2_Api  = "\(baseUrl)trainee/signup_3"
let traineeSignUp_3_Api = "\(baseUrl)trainee/signup_4"
let traineeSignUp_4_Api = "\(baseUrl)trainer/signup_5"





                        //MARK: - TRAINNER API (Instructor)

//profile craetion api --
let trainerSignUpApi = "\(baseUrl)trainer/signup_2"
let trainerSignUp_2_Api = "\(baseUrl)trainer/signup_3"
let trainerGetList = "\(baseUrl)trainer/training/list"
let trainerSignUp_3_Api = "\(baseUrl)trainer/signup_4"

//account detail api --
let accountSaveApi = "\(baseUrl)trainer/bank/add"

//authority detail api --
let authoritySaveApi = "\(baseUrl)trainer/authority/add"

//add card api --
let cardListApi = "\(baseUrl)trainer/card/list"
let addCardApi = "\(baseUrl)trainer/card/add"

//workout Plan--
let addWorkout = "\(baseUrl)trainer/program/add"
let getWorkoutList = "\(baseUrl)trainer/program/list"
let deleteWorkoutList = "\(baseUrl)trainer/program/delete"




