//
//  networkManager.swift
//  FITIT
//
//  Created by Rajneesh Kumar on 07/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import SystemConfiguration

class networkManager: NSObject {
    
   static let networkInstance = networkManager()
    
    func connectionAvailable() -> Bool{
        
        var zeroAddress = sockaddr_in()
        var flags : SCNetworkReachabilityFlags = []
        
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress){
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1){
                zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }

}
