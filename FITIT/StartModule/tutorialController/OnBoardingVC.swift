//
//  OnBoardingVC.swift
//  FITIT
//
//  Created by lokesh chand on 29/08/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit

class OnBoardingVC: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var goButton: UIButton!
    
    
    let imgArry :[UIImage] = [#imageLiteral(resourceName: "tut"),#imageLiteral(resourceName: "tut"),#imageLiteral(resourceName: "tut"),#imageLiteral(resourceName: "tut"),]
    
    let headingArry = ["find the right Coach for you","find the right Coach for you","find the right Coach for you","find the right Coach for you"]
    let descArry = ["Explanatory screens about the applicatio, its functions and how Use it,determine training ,follow up nutrition etc.","Explanatory screens about the applicatio, its functions and how Use it,determine training ,follow up nutrition etc.","Explanatory screens about the applicatio, its functions and how Use it,determine training ,follow up nutrition etc.","Explanatory screens about the applicatio, its functions and how Use it,determine training ,follow up nutrition etc."]
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
          goButton .isHidden = true
    }

    @IBAction func goButtonPressed(_ sender: Any){
        
        let passingController = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(passingController, animated: true)
    }
}


extension OnBoardingVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgArry.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView .dequeueReusableCell(withReuseIdentifier: "OnBoardingCell", for: indexPath) as! OnBoardingCell
        cell.imgView .image = imgArry[indexPath.item]
        cell.headingLbl.text = headingArry[indexPath.item]
        cell.descLbl.text = descArry[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let width = collectionView.frame.width
        let height = collectionView.frame.height
        return CGSize(width: width, height: height)
    }
    
//hanlde the page controll when user swaping ---
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let x = targetContentOffset.pointee.x
        pageController.currentPage = Int(x/collectionView.frame.width)
        if pageController.currentPage == 3{
            goButton .isHidden = false
        }else{
            goButton .isHidden = true
        }
    }
}











