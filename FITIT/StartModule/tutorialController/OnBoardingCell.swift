//
//  OnBoardingCell.swift
//  FITIT
//
//  Created by lokesh chand on 29/08/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit

class OnBoardingCell: UICollectionViewCell
{
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var headingLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    
}
