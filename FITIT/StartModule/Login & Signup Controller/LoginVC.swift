//
//  LoginVC.swift
//  FITIT
//
//  Created by lokesh chand on 29/08/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import  SVProgressHUD
import SwiftyJSON
import CryptoSwift


class LoginVC: UIViewController , UITextFieldDelegate{
    
    @IBOutlet weak var emailTxtFiled: textFieldExt!
    @IBOutlet weak var passTextFiled: textFieldExt!
    
    var is_facebook : Bool = false //this is used for the differentiate user logged from facebook or simple login
    var userEmail : String?
    var userSocialId : String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailTxtFiled .delegate = self
        passTextFiled .delegate = self
    }
    
    //save the user data --
    func saveUserData(responseJson : JSON){
   
        let user_Id = responseJson["data"]["user_id"].stringValue
        let user_Token = responseJson["data"]["user_access_token"].stringValue
        let user_Type = responseJson["data"]["user_type"].stringValue
        let user_Email = responseJson["data"]["user_email"].stringValue
        let user_Phone = responseJson["data"]["user_phone"].stringValue
        let user_Photo = responseJson["data"]["user_profile_pic"].stringValue
        
         UserDefaults .standard .set(user_Id, forKey: userId)
         UserDefaults .standard .set(user_Token, forKey: userToken)
         UserDefaults .standard .set(user_Type, forKey: userLoginType)
         UserDefaults .standard .set(user_Email, forKey: userEmailId)
         UserDefaults .standard .set(user_Phone, forKey: userPhone)
         UserDefaults .standard .set(user_Photo, forKey: userPhoto)
    }
    
//func for the sign up controller  ----
    
    func passSignUpController(){
        let passingController = self.storyboard?.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        
        if is_facebook{
            passingController.is_social = true
        }else{
            passingController.is_social = false
        }
        
        self.navigationController?.pushViewController(passingController, animated: true)
    }

    
//func for the trainningStoryBoard  ----
    
    func passTrainningController(){
        let trainningStoryboard = UIStoryboard(name: "Trainning", bundle: nil)
        let passingController = trainningStoryboard.instantiateViewController(withIdentifier: "trainningNav")
        self.present(passingController, animated: true, completion: nil)
    }
    
    
//func for the instructerStoryBoard -----
    
    func passInstructerController(){
        let trainnerStoryboard = UIStoryboard(name: "trainner", bundle: nil)
        let initailController =  trainnerStoryboard.instantiateViewController(withIdentifier: "TrnrProfileOneController") as! TrnrProfileOneController
        self.present(initailController, animated: true, completion: nil)
    }
    
    
 //texfiled delegate method ---
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}





//MARK: - ALL BUTTON ACTION Block ---

extension LoginVC{
    
    @IBAction func loginBtnPressed(_ sender: Any) {
        
        if emailTxtFiled.text?.count != 0 {
            if validationClass.validationImstance.isValidEmail(email: emailTxtFiled.text!){
                if passTextFiled.text?.count != 0{
                    is_facebook = false
                    loginApiHandling()
                }else{
                    let msg = "Please enter your password."
                    alertClass.alertInstance.showAlertWithMessage(Message: msg, ControllerName: self)
                }
            }else{
                let msg = "Please valid email address."
                alertClass.alertInstance.showAlertWithMessage(Message: msg, ControllerName: self)
            }
            
        }else{
            let msg = "Please enter email id."
            alertClass.alertInstance.showAlertWithMessage(Message: msg, ControllerName: self)
        }
    }
    
    @IBAction func facebookSignPressed(_ sender: Any) {
        faceBookLogin()
    }
    
    @IBAction func enrollmentBtnPressed(_ sender: Any) {
        passSignUpController()
    }
}







//MARK: - ApiHandle Block ---
extension LoginVC  {
    
    func loginApiHandling(){
        
        SVProgressHUD .show(withStatus: "Please wait...")
        var param = [String : Any]()
        
        if is_facebook{
            param["user_email"] = emailTxtFiled.text
            param["user_is_social"] = userSocialId
        }else{
            param["user_email"] = emailTxtFiled.text
            param["user_password"] = passTextFiled.text
        }
        
        apiManager.sharedInstance.handleApiCall(controllerName: self, apiUrl: loginUrlApi, parameter: param, requestType: httpmethod.post) { (apiData) in
            
            if apiData["status"].stringValue == "success"{
                
                SVProgressHUD .showSuccess(withStatus: apiData["message"].stringValue)
                self.saveUserData(responseJson: apiData)
                
                //save the data ---
                
                if apiData["data"]["user_type"].stringValue  == "trainee"{
                    
                    DispatchQueue.main.sync {
                        self.passTrainningController()  }
                }
                else{
                    
                    DispatchQueue.main.sync {
                        self.passInstructerController()  }
                }
            }
            else{
                if self.is_facebook{
                    SVProgressHUD .showSuccess(withStatus: "Yoy have to complte the sign up form.")
                    DispatchQueue.main.sync {
                        self.passSignUpController()  }
                }
                else{
                    SVProgressHUD .showError(withStatus: apiData["message"].stringValue)
                }
                
            }
        }
    }
}








//MARK: - Facebook Login block ---
extension LoginVC  {
    
    //Facebook Method..................................................................
    func faceBookLogin(){
        
        is_facebook = true
        
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.loginBehavior = FBSDKLoginBehavior.browser
        
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self, handler: { (result, error) -> Void in
            if (error == nil){
                
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil{
                    
                    if(fbloginresult.grantedPermissions.contains("email")){
                        self.getFBUserData()
                    }
                }
            }
        })
    }
    
    func getFBUserData(){
        
        if((FBSDKAccessToken.current()) != nil){
            
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    
                    let data = result as! NSDictionary
                    print(data)
                    
                    let profileImgUrl = data .value(forKeyPath: "picture.data.url") as! String
                    
                    let userData = userFacebookData.facebookData
                    userData.socilId = data["id"] as? String
                    userData.userEmail = data["email"] as? String
                    userData.userProfilePic = profileImgUrl
                    
                    self.loginApiHandling()
                }
            })
        }
    }
    
}
