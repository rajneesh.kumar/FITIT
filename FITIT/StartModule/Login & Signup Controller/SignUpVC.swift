//
//  SignUpVC.swift
//  FITIT
//
//  Created by lokesh chand on 29/08/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit
import  SVProgressHUD
import  SwiftyJSON
import CryptoSwift

class SignUpVC: UIViewController , UITextFieldDelegate{

    @IBOutlet weak var segmentControll: UISegmentedControl!
    @IBOutlet weak var emeilTextField: textFieldExt!
    @IBOutlet weak var passTextField: textFieldExt!
    @IBOutlet weak var confirmPassTextField: textFieldExt!

    var userType = "trainee"
    
    
    //this variable is used when user choose facebook login ---
    let userData = userFacebookData.facebookData
    var is_social : Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set the default value ---
        
        if is_social{
            emeilTextField.text = userData.userEmail
        }else{
             emeilTextField.text = ""
        }
    }
    
    //texfiled delegate method ---
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func passController(){
        let passingController =  self.storyboard?.instantiateViewController(withIdentifier: "welcomeController") as! welcomeController
       self.navigationController?.pushViewController(passingController, animated: true)
    }    
}


//MARK: - Api handling ----
extension SignUpVC {
    
    func registerTheUser(){
        
        SVProgressHUD .show(withStatus: "Please wait...")
        var param = [String : Any]()
        
        if is_social{ //social login
           
            param["user_email"] = userData.userEmail
            param["user_password"] = passTextField.text
            param["user_type"] = userType
            param["user_is_social"] = "1"
            param["user_social_id"] = userData.socilId
            param["user_login_src"] = "ios"
            param["user_profile_pic"] = userData.userProfilePic
            
        }
        else{ //normal
            param["user_email"] = emeilTextField.text
            param["user_password"] = passTextField.text
            param["user_type"] = userType
            param["user_is_social"] = "0"
            param["user_social_id"] = ""
            param["user_login_src"] = "ios"
            param["user_profile_pic"] = ""
        }
        
        
        
        apiManager.sharedInstance.handleApiCall(controllerName: self, apiUrl: signupUrlApi, parameter: param, requestType: httpmethod.post) { (apiData) in
            
            if apiData["status"].stringValue == "success"
            {
                SVProgressHUD .showSuccess(withStatus: apiData["message"].stringValue)
                
                //set the data to userdefaults ----
                
                UserDefaults.standard.set(apiData["data"]["user_id"].stringValue, forKey: userId)
                UserDefaults.standard.set(apiData["data"]["user_access_token"].stringValue, forKey: userToken)
                UserDefaults.standard.set(apiData["data"]["user_type"].stringValue, forKey: userLoginType)
                
                // pass to controller ----
                DispatchQueue.main.async{
                    self.passController()
                }
            
            }
            else{
                SVProgressHUD .showError(withStatus: apiData["message"].stringValue)
            }
        }
    }
}





//MARK: - All button action here ----
extension SignUpVC {
    
    @IBAction func signUpBtnPressed(_ sender: Any) {
    
       self.passController()
//        if emeilTextField.text?.count != 0 {
//            if validationClass.validationImstance.isValidEmail(email: emeilTextField.text!){
//                if passTextField.text?.count != 0{
//                    if confirmPassTextField .text?.count != 0{
//                        if passTextField .text == confirmPassTextField .text{
//
//                            registerTheUser()
//
//                        }else{
//                            let msg = "Password and Confirm password must be same."
//                            alertClass.alertInstance.showAlertWithMessage(Message: msg, ControllerName: self)
//                        }
//                    }else{
//                        let msg = "Please enter your confirm."
//                        alertClass.alertInstance.showAlertWithMessage(Message: msg, ControllerName: self)
//                    }
//
//                }else{
//                    let msg = "Please enter your password."
//                    alertClass.alertInstance.showAlertWithMessage(Message: msg, ControllerName: self)
//                }
//            }else{
//                let msg = "Please valid email address."
//                alertClass.alertInstance.showAlertWithMessage(Message: msg, ControllerName: self)
//            }
//        }else{
//            let msg = "Please enter email id."
//            alertClass.alertInstance.showAlertWithMessage(Message: msg, ControllerName: self)
//        }
        
    }
    
    @IBAction func segmentBtnPressed(_ sender: UISegmentedControl) {
        
        if segmentControll.selectedSegmentIndex == 0{
            userType = "trainee"
        }else{
            userType = "trainer"
        }
    }
    
    @IBAction func backbtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
