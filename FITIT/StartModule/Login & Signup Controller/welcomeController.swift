//
//  welcomeController.swift
//  FITIT
//
//  Created by Rajneesh Kumar on 08/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit

class welcomeController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func passTrainningController(){
        
        let trainningStoryboard = UIStoryboard(name: "Trainning", bundle: nil)
        let initailController =  trainningStoryboard.instantiateViewController(withIdentifier: "CreateProfileVC") as! CreateProfileVC
        self.present(initailController, animated: true, completion: nil)
    }
    
    func passInstructerController(){
        
        let trainnerStoryboard = UIStoryboard(name: "trainner", bundle: nil)
        let initailController =  trainnerStoryboard.instantiateViewController(withIdentifier: "TrnrProfileOneController") as! TrnrProfileOneController
        self.present(initailController, animated: true, completion: nil)
    }

   
    @IBAction func doneBtnPressed(_ sender: Any) {
        
        let userType = UserDefaults.standard.string(forKey: userLoginType)
        if userType == "trainee"{
            self.passTrainningController()
            
        }else{
            self.passInstructerController()
        }
        
    }
    
}
