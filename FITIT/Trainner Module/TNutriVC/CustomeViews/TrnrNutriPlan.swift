//
//  TrnrNutriPlanVC.swift
//  FITIT
//
//  Created by Pawan Yadav on 25/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit

class TrnrNutriPlan: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var nameMealTxtfield: UITextField!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initalSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initalSetup()
    }
    
    @IBAction func addFoodDrinkBtn(_ sender: Any) {
        
    }
    
    
    private func initalSetup(){
        Bundle.main.loadNibNamed("TrnrNutriPlan", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth , .flexibleHeight]
    }
    

}
