//
//  TrnrNutriProgramsViewController.swift
//  FITIT
//
//  Created by Pawan Yadav on 25/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit

class TrnrNutriProgramsViewController: UIViewController {

    @IBOutlet var errorView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}




//MARK: - ALL BUTTON ACTION Block ---
extension TrnrNutriProgramsViewController{
    
    @IBAction func addNutribtn(_ sender: Any) {
        
         let passingController = self.storyboard?.instantiateViewController(withIdentifier: "TrnrCreateNutriPlanVC") as! TrnrCreateNutriPlanVC
        
        self.navigationController?.pushViewController(passingController, animated: true)

    }
}
