//
//  TrnrNutriProgramVC.swift
//  FITIT
//
//  Created by lokesh chand on 11/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit

class TrnrNutriProgramVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    //MARK: - Outlets --

    @IBOutlet var errorUiView: UIView!
    @IBOutlet weak var myTbleview: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    //MARK:- TableView DataSource & Delgate.

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 95
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TrnrNutriProgramCell", for: indexPath) as! TrnrNutriProgramCell
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
}

//MARK: - ALL BUTTON ACTION --
    
extension TrnrNutriProgramVC{
        
    @IBAction func addPreesedBtn(_ sender: Any)
    {
        let passingController = self.storyboard?.instantiateViewController(withIdentifier: "TrnrCreateNutriPlanVC") as! TrnrCreateNutriPlanVC
        
        self.navigationController?.pushViewController(passingController, animated: true)
     }
    }

