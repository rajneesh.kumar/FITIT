//
//  TrnrCreateNutriPlanVC.swift
//  FITIT
//
//  Created by Pawan Yadav on 25/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit

class TrnrCreateNutriPlanVC: UIViewController, UITableViewDataSource,UITableViewDelegate  , UITextFieldDelegate{

    //MARK: - Outlets --
    @IBOutlet var stackHightConest: NSLayoutConstraint!
    @IBOutlet var tblViewHightConstant: NSLayoutConstraint!
    @IBOutlet var tblViewTopConstant: NSLayoutConstraint!
    @IBOutlet var mealStackView: UIStackView!
    @IBOutlet var contentUIView: UIView!
    @IBOutlet var tableView: UITableView!
    
   
    override func viewDidLoad() {
        super.viewDidLoad()

        print("ajay code")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func initDietView() -> UIView{
        
        let xib = TrnrNutriPlan()
        xib.nameMealTxtfield.delegate = self
        
        xib.frame = mealStackView.bounds
        xib.frame.size = CGSize(width: mealStackView.frame.size.width, height: 120.0)
        return xib
    }
    

    
//MARK:- TableView DataSource & Delgate.
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 45
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier:"TrnrNutriPlanCell", for: indexPath) as! TrnrNutriPlanCell
        
        return cell
    }
}




//MARK: - ALL BUTTON ACTION --

extension TrnrCreateNutriPlanVC{
    
    @IBAction func addMealPressed(_ sender : UIButton){
        
        let mealView = initDietView()
        stackHightConest .constant = 120
        mealStackView.addArrangedSubview(mealView)
    }
    
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    
    
    @IBAction func saveBtnPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
}
