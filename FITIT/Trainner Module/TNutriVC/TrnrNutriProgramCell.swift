//
//  TrnrNutriProgramCell.swift
//  FITIT
//
//  Created by lokesh chand on 11/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit

class TrnrNutriProgramCell: UITableViewCell {

    @IBOutlet var imgView: UIImageView!
    @IBOutlet var caloriesLbl: UILabel!
    @IBOutlet var mealNameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
