//
//  workoutController.swift
//  FITIT
//
//  Created by Rajneesh Kumar on 21/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD


class workoutController: UIViewController {
    
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var leftBtn: UIButton!
    @IBOutlet weak var rightBtn: UIButton!
    
    var trainngArry = [JSON]()
    var is_edited : Bool = false
    
    var selectedId : Int = 0
    var selectedItemArray = [Int]()
    var selectedData = [[String : String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        rightBtn .isHidden = true
        getWorkoutlist()
    }
}



//MARK: - ALL BUTTON ACTION HERE ---
extension workoutController{
    
    @IBAction func leftBtnPressed(_ sender: Any) {
        if is_edited {
            leftBtn .setTitle("עריכה", for: .normal)
            rightBtn.isHidden = true
            is_edited = false
        }
        else{
            leftBtn .setTitle("ביטול", for: .normal)
            rightBtn.isHidden = false
            is_edited = true
        }
        DispatchQueue.main.async {
            self.tblView.reloadData()
        }
    }
    
    @IBAction func rightBtnPressed(_ sender: Any) {
        
        if selectedItemArray.count > 0{
            for i in 0..<selectedItemArray.count {
                
                let dict = ["id": "\(selectedItemArray[i])"]
                selectedData.append(dict)
            }
            deleteWorkout()
        }else{
            alertClass.alertInstance.showAlertWithMessage(Message: "Please select any workout", ControllerName: self)
        }
        
    }
    
    @IBAction func addBtnPressed(_ sender: Any) {
        let passingController = self.storyboard?.instantiateViewController(withIdentifier: "trnrCreateTrainngController") as! trnrCreateTrainngController
        self.navigationController?.pushViewController(passingController, animated: true)
    }
}





//MARK: - TABLE VIEW DELEGATE AND DATASOURCE ---
extension workoutController : UITableViewDataSource , UITableViewDelegate  {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return trainngArry.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if is_edited {
             let cell = Bundle.main.loadNibNamed("workoutDeleteCell", owner: self, options: nil)?.first as! workoutDeleteCell
            
            cell.selectBtn .tag = indexPath.row
            
            let id = self.trainngArry[indexPath.row]["program_id"].intValue
            if selectedItemArray.contains(id){
                cell.selectBtn .setImage(#imageLiteral(resourceName: "tickMark"), for: .normal)
            }else{
                 cell.selectBtn .setImage(nil, for: .normal)
            }
            cell.workDetailLbl .text = self.trainngArry[indexPath.row]["total_exercise"].stringValue
            cell.workImgview .image = #imageLiteral(resourceName: "NoCard")
            cell.workoutLbl .text = self.trainngArry[indexPath.row]["program_name"].stringValue
            return cell
            
        }else{
            let cell = tableView .dequeueReusableCell(withIdentifier: "workoutCell") as! workoutCell
            cell.workDetailLbl .text = self.trainngArry[indexPath.row]["total_exercise"].stringValue
            cell.workImgview .image = #imageLiteral(resourceName: "NoCard")
            cell.workoutLbl .text = self.trainngArry[indexPath.row]["program_name"].stringValue
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let id  = self.trainngArry[indexPath.row]["program_id"].intValue
        
        if selectedItemArray .contains(id){
            let index = selectedItemArray.index(of: id)
            selectedItemArray.remove(at: index!)
        }else{
            selectedItemArray .append(id)
        }
        self.tblView .reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
}




//MARK: ALL API HANDLE ---
extension workoutController{
    
    func deleteWorkout(){
        
        //self.trainngArry.removeAll()
    
        SVProgressHUD .show(withStatus: "Please wait...")
        var param = [String : Any]()
        
        param["user_id"] = UserDefaults.standard .string(forKey: userId)
        param["user_access_token"] = UserDefaults.standard.string(forKey: userToken)
        //convert array object to the json string ----
        do{
            let data =  try JSONSerialization .data(withJSONObject: selectedData, options: [])
            let idStr = String.init(data: data, encoding: String.Encoding.utf8)

           param["program_id"] = idStr
        }
            
        catch{ print("some error to parse") }
        
        apiManager.sharedInstance.handleApiCall(controllerName: self, apiUrl: deleteWorkoutList, parameter: param, requestType: httpmethod.post) { (apiData) in
            
            if apiData["status"].stringValue == "success"{
                
                SVProgressHUD .showSuccess(withStatus: apiData["message"].stringValue)
                self.trainngArry = apiData["data"].arrayValue
                
                // pass to controller ----
                DispatchQueue.main.async{
                    self.errorView .isHidden = true
                    self.tblView.reloadData()
                }
            }
            else{
                DispatchQueue.main.async{
                    self.errorView .isHidden = true
                }
                SVProgressHUD .showError(withStatus: apiData["message"].stringValue)
            }
        }
    }
    
    func getWorkoutlist(){
        
        self.trainngArry.removeAll()
        
        SVProgressHUD .show(withStatus: "Please wait...")
        var param = [String : Any]()
        
        param["user_id"] = UserDefaults.standard .string(forKey: userId)
        param["user_access_token"] = UserDefaults.standard.string(forKey: userToken)

        apiManager.sharedInstance.handleApiCall(controllerName: self, apiUrl: getWorkoutList, parameter: param, requestType: httpmethod.post) { (apiData) in
            
            if apiData["status"].stringValue == "success"{
                
                SVProgressHUD .showSuccess(withStatus: apiData["message"].stringValue)
                self.trainngArry = apiData["data"].arrayValue
                
                // pass to controller ----
                DispatchQueue.main.async{
                    self.errorView .isHidden = true
                    self.tblView.reloadData()
                }
            }
            else{
                DispatchQueue.main.async{
                    self.errorView .isHidden = true
                }
                SVProgressHUD .showError(withStatus: apiData["message"].stringValue)
            }
        }
    }
    
    
}
