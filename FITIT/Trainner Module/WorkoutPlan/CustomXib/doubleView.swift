//
//  doubleView.swift
//  FITIT
//
//  Created by Rajneesh Kumar on 21/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit

class doubleView: UIView {

   
    @IBOutlet var contentView: UIView!
    
    @IBOutlet weak var rlbl: UILabel!
    @IBOutlet weak var rMinusBtn: UIButton!
    @IBOutlet weak var rplusBtn: UIButton!
    
    @IBOutlet weak var lLbl: UILabel!
    @IBOutlet weak var lMinusBtn: UIButton!
    @IBOutlet weak var lplusBtn: UIButton!
    
    @IBOutlet weak var removeBtn: UIButton!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initalSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initalSetup()
    }
    
    private func initalSetup(){
        
        Bundle.main.loadNibNamed("doubleView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth , .flexibleHeight]
    }
}

