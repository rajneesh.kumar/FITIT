//
//  singleView.swift
//  FITIT
//
//  Created by Rajneesh Kumar on 21/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit

class singleView: UIView {

    @IBOutlet var contentView: UIView!
    
    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var removeBtn: UIButton!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initalSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initalSetup()
    }
    
    private func initalSetup(){
        
        Bundle.main.loadNibNamed("singleView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth , .flexibleHeight]
    }

}
