//
//  trnrCreateTrainngController.swift
//  FITIT
//
//  Created by Rajneesh Kumar on 21/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SVProgressHUD

class trnrCreateTrainngController: UIViewController , UITextFieldDelegate, add_Workout {
    
    
    @IBOutlet weak var commentTxtView: ExtViewText!
    @IBOutlet weak var addView: ExtView!
    @IBOutlet weak var programTxtField: textFieldExt!
    @IBOutlet weak var equipmentTxt: textFieldExt!
    
    
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var tblHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var tblTopConst: NSLayoutConstraint!
    
    var workoutData = [[String : Any]]()
    
    var tapGesture = UITapGestureRecognizer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        gestureMethod(view: addView)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func saveWorkout(data: [String : Any]) {
        if data.count > 0{
            do{
                let data =  try JSONSerialization .data(withJSONObject: data, options: [])
                do{
                    let trainningStr = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    workoutData.append(trainningStr as! [String : Any])
                }catch { }
                
            }catch { }
        }
        
        //set the table view height --
        
        tblTopConst .constant = 15.0
        tblHeightConstant .constant = CGFloat(workoutData.count * 60)
        
        DispatchQueue.main.async {
            self.tblView.reloadData()
        }
    }
    
}



//MARK: - HANLDE TAP GESTURE --
extension trnrCreateTrainngController{
    
    func gestureMethod(view : UIView){
        
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        view.isUserInteractionEnabled = true
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapView))
        view.addGestureRecognizer(tapGesture)
    }
    
    
    @objc func handleTapView(_ sender:UITapGestureRecognizer){
        let passingControler = self.storyboard?.instantiateViewController(withIdentifier: "trnrExerciseController") as! trnrExerciseController
        passingControler.worloutDelegate = self
        self.navigationController?.pushViewController(passingControler, animated: true)
    }
}





//MARK: - ALL BUTOTN ACTION HERE --
extension trnrCreateTrainngController {
    
    @IBAction func saveBtnPressed(_ sender: Any) {
        saveExerciseData()
    }
    
    @IBAction func backBtnPresesd(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}




//MARK: - ALL BUTOTN ACTION HERE --
extension trnrCreateTrainngController  : UITableViewDelegate , UITableViewDataSource{

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return workoutData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "exerciseCell") as! exerciseCell
        
        let data = workoutData[indexPath.row]
        cell.excersizelLbl .text = data["exercise_name"] as? String
        return cell
    }
}








//MARK: - ALL API HERE --
extension trnrCreateTrainngController {
    
    func saveExerciseData(){
        
        SVProgressHUD .show(withStatus: "Please wait...")
        
        var param = [String : Any]()
        param["user_id"] = UserDefaults.standard.string(forKey: userId)
        param["user_access_token"] = UserDefaults.standard.string(forKey: userToken)
        param["equipment"] = equipmentTxt.text
        param["notes"] = commentTxtView.text
        param["program_name"] = programTxtField.text
        
        //convert array object to the json string ----
        do{
            let data =  try JSONSerialization .data(withJSONObject: workoutData, options: [])
            let exerciseStr = String.init(data: data, encoding: String.Encoding.utf8)
            param["exercise_list"] = exerciseStr
            //print(exerciseStr)
        } catch { }
        
        print(param)
        
        apiManager.sharedInstance.handleApiCall(controllerName: self, apiUrl: addWorkout, parameter: param, requestType: httpmethod.post) { (apiData) in
            
            if apiData["status"].stringValue == "success"{
                SVProgressHUD .showSuccess(withStatus: apiData["message"].stringValue)
                
                DispatchQueue.main.async {
                    
                }
            }
            else{
                SVProgressHUD .showError(withStatus: apiData["message"].stringValue)
            }
        }
    }
}



