//
//  trnrExerciseController.swift
//  FITIT
//
//  Created by Rajneesh Kumar on 21/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit


protocol add_Workout {
    func saveWorkout(data : [String : Any])
}

class trnrExerciseController: UIViewController , UITextFieldDelegate {
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var timeTxtField: textFieldExt!
    @IBOutlet weak var bodyTxtFiled: textFieldExt!
    @IBOutlet weak var exerciseNmeTxt: textFieldExt!
    @IBOutlet weak var periodTxtFiled: textFieldExt!
    @IBOutlet weak var restTxtfiled: textFieldExt!
    @IBOutlet weak var commnetTxtView: ExtViewText!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var pickerBackvIEW: UIView!
    
    @IBOutlet weak var optionLbl: UILabel!
    
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var plusImg: UIImageView!
    
    
    //HeightConst ---
    @IBOutlet weak var timeHeightConst: NSLayoutConstraint!
    @IBOutlet weak var timeTopConst: NSLayoutConstraint!
    
    @IBOutlet weak var satckHeightConst: NSLayoutConstraint!
    
    @IBOutlet weak var periodHeightConst: NSLayoutConstraint!
    @IBOutlet weak var periodTopConst: NSLayoutConstraint!
    
    @IBOutlet weak var scrollHeightConst: NSLayoutConstraint!
    
    
    //protocol varibale --
    var worloutDelegate : add_Workout?
    
    //save all the data
    var exerciseObj : AnyObject?
    var trainnerSet = [Int : [AnyObject]]()
    
    var setdict = [String : Any]()
    var dataDict = [String : Any]()
    var setDictArray = [[String : Any]]()
    
    var selectedTag : Int = 0
    var excerciseCount : Int = 0
    
    var tempArry = [Int]()
    
    //store all the objects of view --
    var repeatArray = [singleView]()
    var weightArray = [doubleView]()
    var timeArray = [singleView]()
    
    let excersizeType = ["Type set" , "Repetition and weight" , "Time"]

    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}


//MARK: - ALL BTN ACTION  --
extension trnrExerciseController{
    
    @IBAction func setBtnPresed(_ sender: Any) {
        self.pickerBackvIEW .isHidden = false
    }
    
    @IBAction func nextBtnPressed(_ sender: Any) {
        
        setdict.removeAll()
        dataDict.removeAll()
        
        dataDict["exercise_name"] = exerciseNmeTxt.text
        dataDict["area_body"] = bodyTxtFiled .text
        
        //get all the address and bind the data --
        let keyCollection = trainnerSet.keys
        let keyArray = Array(keyCollection)
        
        if keyArray.contains(selectedTag){
            
            if selectedTag == 0{
                let objArray = trainnerSet[selectedTag]
                
                for i in 0 ..< objArray!.count{
                    if selectedTag == 0 || selectedTag == 2{}
                    exerciseObj = objArray![i] as! singleView
                    let obj = exerciseObj as! singleView
                    
                    if selectedTag == 0{
                        setdict["repeat"] = obj.lbl.text
                        setdict["weight"] = ""
                        setdict["time"] = ""
                    }else{
                        setdict["repeat"] = ""
                        setdict["weight"] = ""
                        setdict["time"] = obj.lbl.text
                    }
                    
                    getJsonObject(valueDict: setdict)
                }
            }
            else if selectedTag == 1{
                
                let objArray = trainnerSet[selectedTag]
                
                for i in 0 ..< objArray!.count{
                    if selectedTag == 0 || selectedTag == 2{}
                    exerciseObj = objArray![i] as! doubleView
                    let obj = exerciseObj as! doubleView
                    
                    setdict["repeat"] = obj.rlbl.text
                    setdict["weight"] = obj.lLbl.text
                    setdict["time"] = ""
                    
                     getJsonObject(valueDict: setdict)
                }
            }
        }
    
        dataDict["set_list"] = setDictArray
        
        if selectedTag == 0{
            dataDict["set_type"] = "repeat"
            dataDict["rest_time"] = restTxtfiled.text
            dataDict["by_time"] = ""
            dataDict["exercise_period"] = ""
            
        }else if selectedTag == 1{
            dataDict["set_type"] = "repeat_weight"
            dataDict["rest_time"] = restTxtfiled.text
            dataDict["by_time"] = ""
            dataDict["exercise_period"] = periodTxtFiled.text
            
        }else{
            dataDict["set_type"] = "time"
            dataDict["rest_time"] = ""
            dataDict["by_time"] = timeTxtField.text
            dataDict["exercise_period"] = periodTxtFiled.text
        }
       // print(dataDict)
        
        worloutDelegate?.saveWorkout(data: dataDict)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doneBtnPressed(_ sender: Any) {
        self.pickerBackvIEW.isHidden = true
    }
    
}





//MARK: - ALL CUSTEM METHOD ---
extension trnrExerciseController{
    
    
    //get json object ---
    
    func getJsonObject(valueDict : [String : Any]){
        //convert all the set dict to json object ---
        if valueDict.count > 0{
            do{
                let data =  try JSONSerialization .data(withJSONObject: setdict, options: [])
                do{
                    let setStr = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    setDictArray.append(setStr as! [String : Any])
                }catch { }
            }catch { }
        }
    }
    
    
    
    //single set method ---
    
    @objc func minusBtnPressed(_ sender : UIButton){
        
        if selectedTag == 0{
            let objArry = trainnerSet[0]
            exerciseObj = objArry![sender.tag] as! singleView
            let obj = exerciseObj as! singleView
            
            //first get previous value of lbl then minus --
            guard let set = obj.lbl.text else { return }
            let intSet = Int(set)
            obj.lbl .text = "\(intSet! - 1)"
        }
        else if selectedTag == 2{
            let objArry = trainnerSet[2]
            exerciseObj = objArry![sender.tag] as! singleView
            let obj = exerciseObj as! singleView
            
            //first get previous value of lbl then minus --
            guard let set = obj.lbl.text else { return }
            let intSet = Int(set)
            obj.lbl .text = "\(intSet! - 1)"
        }
    }
    
    
    @objc func plusBtnPresed(_ sender : UIButton){
        if selectedTag == 0{
            let objArry = trainnerSet[0]
            exerciseObj = objArry![sender.tag] as! singleView
            let obj = exerciseObj as! singleView
            
            //first get previous value of lbl then minus --
            guard let set = obj.lbl.text else { return }
            let intSet = Int(set)
            obj.lbl .text = "\(intSet! + 1)"
        }
        else if selectedTag == 2{
            let objArry = trainnerSet[2]
            exerciseObj = objArry![sender.tag] as! singleView
            let obj = exerciseObj as! singleView
            
            //first get previous value of lbl then minus --
            guard let set = obj.lbl.text else { return }
            let intSet = Int(set)
            obj.lbl .text = "\(intSet! + 1)"
        }
    }
    
    
    //double set method ----
    
    @objc func rMinusBtnPressed(_ sender : UIButton){
        
        if selectedTag == 1{
            let objArry = trainnerSet[1]
            exerciseObj = objArry![sender.tag] as! doubleView
            let obj = exerciseObj as! doubleView
            
            //first get previous value of lbl then minus --
            guard let set = obj.rlbl.text else { return }
            let intSet = Int(set)
            obj.rlbl .text = "\(intSet! - 1)"
        }
    }
    
    
    @objc func rPlusBtnPresed(_ sender : UIButton){
        if selectedTag == 1{
            let objArry = trainnerSet[1]
            exerciseObj = objArry![sender.tag] as! doubleView
            let obj = exerciseObj as! doubleView
            
            //first get previous value of lbl then minus --
            guard let set = obj.rlbl.text else { return }
            let intSet = Int(set)
            obj.rlbl .text = "\(intSet! + 1)"
        }
    }
    
    @objc func lMinusBtnPressed(_ sender : UIButton){
        
        if selectedTag == 1{
            let objArry = trainnerSet[1]
            exerciseObj = objArry![sender.tag] as! doubleView
            let obj = exerciseObj as! doubleView
            
            //first get previous value of lbl then minus --
            guard let set = obj.lLbl.text else { return }
            let intSet = Int(set)
            obj.lLbl .text = "\(intSet! - 1)"
        }
    }
    
    
    @objc func lPlusBtnPresed(_ sender : UIButton){
        if selectedTag == 1{
            let objArry = trainnerSet[1]
            exerciseObj = objArry![sender.tag] as! doubleView
            let obj = exerciseObj as! doubleView
            
            //first get previous value of lbl then minus --
            guard let set = obj.lLbl.text else { return }
            let intSet = Int(set)
            obj.lLbl .text = "\(intSet! + 1)"
        }
    }
    
    
    
    @objc func removeBtn(_ sender : UIButton){
        let tag = sender.tag
        if selectedTag == 0{
            var objArry = trainnerSet[0]
            objArry?.remove(at: tag)
            trainnerSet[0]?.removeAll()
            trainnerSet[0] = objArry
        }
        else if selectedTag == 1{
            
        }else{
            var objArry = trainnerSet[2]
            objArry?.remove(at: tag)
            trainnerSet[2]?.removeAll()
            trainnerSet[2] = objArry
        }
         handleExerciseView()
    }
}




//MARK: - PICKER FUNCTIONALITY DEFINED ---
extension trnrExerciseController : UIPickerViewDelegate , UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return excersizeType.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return excersizeType[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let currentItem = excersizeType[row]
        optionLbl.text = currentItem
        
        let currentIndex = excersizeType.index(of: currentItem)
        selectedTag = currentIndex!
        
        //reset all the value
        tempArry.removeAll()
        excerciseCount = 0
        
        guard let index = currentIndex  else { return }
        
        switch index {
        case 0:
            timeTopConst .constant = 0
            timeHeightConst .constant = 0
            
            periodTopConst .constant = 0
            periodHeightConst .constant = 0
            
            removeAllView(tag: 0)
            handleExerciseView()
            break
            
        case 1:
            timeTopConst .constant = 0
            timeHeightConst .constant = 0
            
            periodTopConst .constant = 10
            periodHeightConst .constant = 45
            
            removeAllView(tag: 1)
            handleExerciseView()
            break
            
        case 2:
            timeTopConst .constant = 10
            timeHeightConst .constant = 45
            
            periodTopConst .constant = 10
            periodHeightConst .constant = 45
            
            removeAllView(tag: 2)
            handleExerciseView()
            break
            
        default:
            break
        }
    }
}





//MARK: - HANLDE VIEW ADDED ---
extension trnrExerciseController{
    
    @IBAction func addSetView(_ sender: Any) {
        
        excerciseCount += 1
        
        for i in 0 ..< excerciseCount{
            
            //add view to the tempArray for check already any view is available or not
            if tempArry .contains(i){
                continue
            }else{
                tempArry.append(i)
                if selectedTag == 0 || selectedTag == 2{
                    let repeatView = initSingleView(tag: i)
                    stackView.addArrangedSubview(repeatView)
                }else{
                    let timeView = initDoubleView(tag: i)
                    stackView.addArrangedSubview(timeView)
                }
                
                //adjust the height of stack view ----
                
                let stkHeight = CGFloat(48 * (i+1))
                //let scrollHeight = scrollViewHight + CGFloat(250 * (i+1))
                satckHeightConst.constant = stkHeight
                scrollHeightConst.constant = 690 + stkHeight
            }
        }
    }
    

    func handleExerciseView(){
        if let trainnerArray = trainnerSet[selectedTag]{
            
            for i in 0 ..< trainnerArray.count{
                if selectedTag == 0 || selectedTag == 2{
                    exerciseObj = trainnerArray[i] as? singleView
                }else{
                    exerciseObj = trainnerArray[i] as? doubleView
                }
                stackView.addArrangedSubview(exerciseObj as! UIView)
                let stkHeight = CGFloat(48 * (i+1))
                satckHeightConst .constant = stkHeight
                excerciseCount += 1
                tempArry.append(i)
            }
        }
        else{
            addSetView(self)
        }
    }
    
    
    
    func initSingleView(tag : Int) -> UIView{
        
        let xib = singleView()
        
        xib.minusBtn .tag = tag
        xib.plusBtn .tag = tag
        xib.removeBtn .tag = tag
        
        xib.removeBtn .addTarget(self, action: #selector(removeBtn(_:)), for: .touchUpInside)
        xib.minusBtn .addTarget(self, action: #selector(minusBtnPressed), for: .touchUpInside)
        xib.plusBtn .addTarget(self, action: #selector(plusBtnPresed), for: .touchUpInside)
        
        xib.frame = stackView.bounds
        xib.frame.size = CGSize(width: stackView.frame.size.width, height: 45.0)
        
        
        if selectedTag == 0{
            repeatArray .append(xib)
        }else{
            timeArray.append(xib)
        }
        
        if let keyExit = trainnerSet[selectedTag]{
            var previousArray = keyExit
            previousArray .append(xib)
            trainnerSet[selectedTag] = previousArray
            
            if selectedTag == 0{
                repeatArray.removeAll()
            }else{
                timeArray.removeAll()
            }
        }
        else{
            //now add the value for upper key that we made --
            if selectedTag == 0{
                trainnerSet[selectedTag] = repeatArray
                repeatArray.removeAll()
            }else{
                trainnerSet[selectedTag] = timeArray
                timeArray.removeAll()
            }
        }
        return xib
    }
    
    
    
    
    func initDoubleView(tag : Int) -> UIView{
        
        let xib = doubleView()
        
        xib.rplusBtn .tag = tag
        xib.rMinusBtn .tag = tag
        
        xib.lplusBtn .tag = tag
        xib.lMinusBtn .tag = tag
        
        xib.removeBtn .tag = tag
        
        xib.lMinusBtn .addTarget(self, action: #selector(lMinusBtnPressed(_:)), for: .touchUpInside)
        xib.lplusBtn .addTarget(self, action: #selector(lPlusBtnPresed(_:)), for: .touchUpInside)
        
        xib.rMinusBtn .addTarget(self, action: #selector(rMinusBtnPressed(_:)), for: .touchUpInside)
        xib.rplusBtn .addTarget(self, action: #selector(rPlusBtnPresed(_:)), for: .touchUpInside)
        
        xib.removeBtn.addTarget(self, action: #selector(removeBtn(_:)), for: .touchUpInside)
        
        
        weightArray.append(xib)
        
        if let keyExit = trainnerSet[selectedTag]{
            var previousArray = keyExit
            previousArray .append(xib)
            trainnerSet[selectedTag] = previousArray
            
            weightArray.removeAll()
        }else{
            //now add the value for upper key that we made --
            trainnerSet[selectedTag] = weightArray
            weightArray.removeAll()
        }
        
        xib.frame = stackView.bounds
        xib.frame.size = CGSize(width: stackView.frame.size.width, height: 45.0)
        
        return xib
    }
    
    func removeAllView(tag : Int){
        
        //get the key for find value --
        let keyCollection = trainnerSet.keys
        let keyArray = Array(keyCollection)
        
        for i in 0 ..< keyArray.count{
            let key = keyArray[i]
            if key != tag{
                if let setTypeArray = trainnerSet[key]{
                    
                    for j in 0 ..< setTypeArray.count{
                        let obj = setTypeArray[j]
                        obj.removeFromSuperview()
                    }
                }
            }
        }
    }
}


