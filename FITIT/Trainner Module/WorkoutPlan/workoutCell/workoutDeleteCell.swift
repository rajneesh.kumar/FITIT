//
//  workoutDeleteCell.swift
//  FITIT
//
//  Created by Rajneesh Kumar on 25/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit

class workoutDeleteCell: UITableViewCell {
    
    @IBOutlet weak var workDetailLbl: UILabel!
    @IBOutlet weak var workoutLbl: UILabel!
    @IBOutlet weak var workImgview: UIImageView!
   
    @IBOutlet weak var selectBtn: UIButton!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
