//
//  trnrCustemCell.swift
//  FITIT
//
//  Created by Rajneesh Kumar on 13/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit

class trnrCustemCell: UITableViewCell {
    
    
    //gallry view cell outlets ---
    
    @IBOutlet weak var detailLbl: UILabel!
    @IBOutlet weak var imgvIEW: UIImageView!
    @IBOutlet weak var arrowImg: UIImageView!
    @IBOutlet weak var lineLbl: UILabel!
    
    
    //add card view cell oytlets ---
    @IBOutlet weak var cardName: UILabel!
    @IBOutlet weak var cardImg: ExtImg!
    @IBOutlet weak var rupeeLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
