//
//  trnrCardContoller.swift
//  FITIT
//
//  Created by Rajneesh Kumar on 13/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit
import  SwiftyJSON
import SVProgressHUD
import SDWebImage

class trnrCardContoller: UIViewController {
    
    @IBOutlet weak var cardTblView: UITableView!
    @IBOutlet weak var errorView: UIView!
    
    var cardArry = [JSON]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.getCardList()
    }
    
    func passToAddCardContoller(){
        let passingController = self.storyboard?.instantiateViewController(withIdentifier: "addCardController") as! addCardController
        self.navigationController?.pushViewController(passingController, animated: true)
    }

    @IBAction func addBtnPressed(_ sender: Any) {
        passToAddCardContoller()
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}



//MARK: - TABLE VIEW DELEGATE AND DATASOURCE METHOD ---
extension trnrCardContoller : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return cardArry.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView .dequeueReusableCell(withIdentifier: "trnrCustemCell", for: indexPath) as! trnrCustemCell
        
        cell.cardName .text = cardArry[indexPath.row]["card_name"].stringValue
        let price = cardArry[indexPath.row]["training_wages"].stringValue
        let trainingType = cardArry[indexPath.row]["training_option"].stringValue
        cell.rupeeLbl .text = "₪\(price) " + trainingType
        
        //get the image --
        let imgStr =  cardArry[indexPath.row]["icon"].stringValue
        
        if imgStr != ""{ 
            let imgUrl = URL(string:imgStr)
            cell.cardImg.sd_setImage(with: imgUrl, placeholderImage: #imageLiteral(resourceName: "transctionCard"))
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let passingController = self.storyboard?.instantiateViewController(withIdentifier: "addCardController") as! addCardController
        
        passingController.is_passed = true
        passingController.trainningOption = cardArry[indexPath.row]["training_option"].stringValue
        passingController.cardName = cardArry[indexPath.row]["card_name"].stringValue
        passingController.cardId = cardArry[indexPath.row]["card_id"].stringValue
        passingController.trainningId = cardArry[indexPath.row]["training_typeid_fk"].stringValue
        passingController.locationId = cardArry[indexPath.row]["location_id_fk"].stringValue
        passingController.trainningDuration = cardArry[indexPath.row]["training_duration"].stringValue
        passingController.participents = cardArry[indexPath.row]["participants"].stringValue
        passingController.priceTrainning = cardArry[indexPath.row]["training_wages"].stringValue
        
        self.navigationController?.pushViewController(passingController, animated: true)
        
    }
}


//MARK: - API HABDLE--
extension trnrCardContoller{
    
    func getCardList(){
        
        self.cardArry .removeAll()
        SVProgressHUD .show(withStatus: "Please wait...")
        
        var someDict = [String : Any]()
        someDict["user_id"] = UserDefaults.standard .string(forKey: userId)
        someDict["user_access_token"] = UserDefaults .standard .string(forKey: userToken)
        
       // someDict["card_id"] = ""
        
        
        apiManager.sharedInstance.handleApiCall(controllerName: self, apiUrl: cardListApi, parameter: someDict, requestType: httpmethod.post) { (responseData) in
            
            if responseData["status"].stringValue == "success"{
                SVProgressHUD.showSuccess(withStatus: responseData["message"].stringValue)
                
                self.cardArry = responseData["data"].arrayValue
               
                DispatchQueue.main.async {
                    self.cardTblView.isHidden = false
                    self.errorView .isHidden = true
                    self.cardTblView.reloadData()
                }
            }
            else{
                DispatchQueue.main.async{
                    self.errorView .isHidden = false
                    self.cardTblView .isHidden = true
                }
                SVProgressHUD.showError(withStatus: responseData["message"].stringValue)
            }
        }
    }
    
    
}
