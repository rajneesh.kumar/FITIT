//
//  addCardController.swift
//  FITIT
//
//  Created by Rajneesh Kumar on 13/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON



struct traingTypeData {
    var training_id : String?
    var training_type : String?
    var training_icon : String?
}

struct addressData {
    var location_id : String?
    var location_name : String?
}


class addCardController: UIViewController , UITextFieldDelegate {

    @IBOutlet weak var cardNameTxtFiled: textFieldExt!
    @IBOutlet weak var hourTxtFiled: textFieldExt!
    @IBOutlet weak var priceTxtFiled: textFieldExt!
    @IBOutlet weak var participantesTxtFiled: textFieldExt!
    
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var trainningLbl: UILabel!
    
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var pickerBackView: UIView!
    
    @IBOutlet weak var collectTopConst: NSLayoutConstraint!
    @IBOutlet weak var collectHeightConst: NSLayoutConstraint!
    

    
    var trainningOption : String = "personal"  //set the by default trainning option
    var locationId : String = ""
    var trainningId : String = ""
    var cardName : String = ""
    var trainningDuration : String = ""
    var priceTrainning : String = ""
    var participents : String = ""
    var cardId : String = ""
    
    var is_passed : Bool = false
    
    
    
    var tapGesture = UITapGestureRecognizer()
    
    var trainningTypeArray = [traingTypeData]()
    var locationDataArray = [addressData]()
    
    var trainningList = [[String : String]]()
    
    var pickerCurrentTag : Int = 1
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        initDataSetUp()
        initTapGesture(defaultSelection: 12)

        //api call for the get master data ---
        getMaterList(masterUrl: locationListApi, tag: 1)
        getMaterList(masterUrl: trainningTypeListApi, tag: -1)
    }
    
// init data setup --
    func initDataSetUp(){
        
        //set the value if there is already value in any card --
        
        cardNameTxtFiled .text = cardName
        locationLbl.text = locationId
        trainningLbl .text = trainningId
        hourTxtFiled .text = trainningDuration
        priceTxtFiled .text = priceTrainning
        participantesTxtFiled .text = participents
        
        if trainningOption == "personal"{
            initTapGesture(defaultSelection: 12)
        }else if trainningOption == "double"{
            initTapGesture(defaultSelection: 11)
        }else{
              initTapGesture(defaultSelection: 10)
             initMemberTxt()
        }
    }
    
//add tap geture on all img view --
    func initTapGesture(defaultSelection : Int){
        
        for i in 10 ..< 13{
            let view = self.view.viewWithTag(i) as! ExtImg
            if i == defaultSelection{
                view.layer .borderWidth = 3.0
                view.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.3607843137, blue: 1, alpha: 1)
            }else{
                view.layer .borderWidth = 0.0
                view.layer.borderColor = UIColor.clear.cgColor
            }
            gestureMethod(view: view)
        }
    }
    
    func initMemberTxt(){
        collectTopConst .constant = 10.0
        collectHeightConst .constant = 40.0
    }
    
    func deinitMemberTxt(){
        collectTopConst .constant = 0.0
        collectHeightConst .constant = 0.0
    }
    
    //textfiled delegate method ---
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    @IBAction func submitBtnPressed(_ sender: Any) {
        saveUserData()
    }
}




//MARK: - ALL BTN ACTION ---
extension addCardController{
    
    
    
    @IBAction func doneBtnPresesd(_ sender: Any) {
        self.pickerBackView .isHidden = true
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func trainningBtnPressed(_ sender: Any) {
        pickerCurrentTag = -1
        pickerBackView.isHidden = false
        self.pickerView .reloadAllComponents()

    }
    
    @IBAction func locationBtnPressed(_ sender: Any) {
        pickerCurrentTag = 1
        pickerBackView.isHidden = false
        self.pickerView .reloadAllComponents()

    }
}




//MARK: -  TAP GESTURE HANDLE ---
extension addCardController{
    
    func gestureMethod(view : UIImageView){
        
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        view.isUserInteractionEnabled = true
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapView))
        view.addGestureRecognizer(tapGesture)
    }
    
    
    @objc func handleTapView(_ sender:UITapGestureRecognizer){
        
        let tag = sender.view?.tag
        
        for i in 10 ..< 13{
            if tag == i{
                let currentImg = self.view.viewWithTag(i) as! ExtImg
                currentImg.layer.borderWidth = 3.0
                currentImg.layer.borderColor =  #colorLiteral(red: 0.4392156863, green: 0.3607843137, blue: 1, alpha: 1)
                
            }else{
                let currentImg = self.view.viewWithTag(i) as! ExtImg
                currentImg.layer.borderWidth = 0.0
                currentImg.layer.borderColor = UIColor.clear.cgColor
            }
        }
        
        switch tag {
        case 12:
            trainningOption = "personal"
            deinitMemberTxt()
            break
        case 11:
            trainningOption = "double"
            deinitMemberTxt()
            break
        case 10:
            trainningOption = "collective"
            initMemberTxt()
            break
        default:
            trainningOption = ""
            break
        }
    }
}


//MARK: - Picker VIEW DELEGATE AND DATSOURCE  --
extension addCardController : UIPickerViewDelegate , UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if  pickerCurrentTag > 0 && locationDataArray .count > 0{
            return locationDataArray.count
        }else if pickerCurrentTag < 0 &&  trainningTypeArray .count > 0{
            return trainningTypeArray.count
        }
        else{
            return 0
        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerCurrentTag > 0{
            let locationType = locationDataArray[row]
            return locationType.location_name
        }
        else{
            let trinningType = trainningTypeArray[row]
            return trinningType.training_type
        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerCurrentTag > 0{
         let locationType = locationDataArray[row]
         locationLbl .text = locationType.location_name
            locationId = locationType.location_id!
        }
        else{
            let traineeType = trainningTypeArray[row]
            trainningLbl .text = traineeType.training_type
             trainningId = traineeType.training_id!
        }
    }
}



//MARK: - ALL API HANDLE --
extension addCardController {
    
//get location list and trainning type list ---
    func getMaterList(masterUrl : String , tag : Int){
        
        SVProgressHUD.show(withStatus: "Please wait...")
        
        var param = [String : Any]()
        param["user_id"] = UserDefaults.standard .string(forKey: userId)
        param["user_access_token"] = UserDefaults.standard .string(forKey: userToken)
        
        apiManager.sharedInstance.handleApiCall(controllerName: self, apiUrl: masterUrl, parameter: param, requestType: httpmethod.post) { (responseJSON) in
            
            if responseJSON["status"] == "success"{
                
                if tag > 0{
                    self.locationDataArray.removeAll()
                    let dataArray = responseJSON["data"].arrayValue
                    
                    for i in 0 ..< dataArray.count{
                        let id = dataArray[i]["location_id"].stringValue
                        let name = dataArray[i]["location_name"].stringValue
                        let locations = addressData(location_id: id, location_name: name)
                        self.locationDataArray.append(locations)
                    }
                }else{
                    
                    self.trainningTypeArray.removeAll()
                    let trainerArray = responseJSON["data"].arrayValue
                    
                    for i in 0 ..< trainerArray.count{
                        let id = trainerArray[i]["training_id"].stringValue
                        let name = trainerArray[i]["training_type"].stringValue
                        let img = trainerArray[i]["training_icon"].stringValue
                        
                        let trinneTypes = traingTypeData(training_id: id, training_type: name, training_icon: img)
                        self.trainningTypeArray.append(trinneTypes)
                    }
                }
            }
            SVProgressHUD .dismiss()
        }
    }
    
    
    func saveUserData(){
        SVProgressHUD .show(withStatus: "Please wait...")
        
        var param = [String : Any]()
        param["user_id"] = UserDefaults.standard.string(forKey: userId)
        param["user_access_token"] = UserDefaults.standard.string(forKey: userToken)
        param["training_option"] = trainningOption
        param["training_typeid"] = trainningId
        param["location_id"] = locationId
        param["training_duration"] = hourTxtFiled.text
        param["training_wages"] = priceTxtFiled.text
        param["card_name"] = cardNameTxtFiled.text
        
        if trainningOption == "collective"{
            param["participants"] = participantesTxtFiled.text
        }
        if is_passed{
            param["card_id"] = cardId
        }
        
        apiManager.sharedInstance.handleApiCall(controllerName: self, apiUrl: addCardApi, parameter: param, requestType: httpmethod.post) { (apiData) in
            
            if apiData["status"].stringValue == "success"{
                SVProgressHUD .showSuccess(withStatus: apiData["message"].stringValue)
            }
            else{
                SVProgressHUD .showError(withStatus: apiData["message"].stringValue)
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                 self.navigationController?.popViewController(animated: true)
            }
        }
    }
}
