//
//  trnrProfileContoller.swift
//  FITIT
//
//  Created by Rajneesh Kumar on 13/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//



import UIKit


class trnrProfileContoller: UIViewController {
    
    
    @IBOutlet weak var profileTblView: UITableView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    
    let titleArray = ["התנתק", "שאלות ותשובות", "דרג את האפלקצייה", "פרטי חשבון","הסמכות","כרטיסיות אימון","גלריית תמונות","ערוך פרופיל"]
    
    let imageArray = [#imageLiteral(resourceName: "profile"), #imageLiteral(resourceName: "gallary") , #imageLiteral(resourceName: "transctionCard") , #imageLiteral(resourceName: "certificate") , #imageLiteral(resourceName: "account") ,#imageLiteral(resourceName: "fav") , #imageLiteral(resourceName: "questionAMRK") , #imageLiteral(resourceName: "LOGOUT")]

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    
//all passing controller method --
    
    func passToCard(){
        let passingController = self.storyboard?.instantiateViewController(withIdentifier: "trnrCardContoller") as! trnrCardContoller
        self.navigationController?.pushViewController(passingController, animated: true)
    }
    
    func passToGallry(){
        let passingController = self.storyboard?.instantiateViewController(withIdentifier: "TrnrMyProfilePhotoEditVC") as! TrnrMyProfilePhotoEditVC
        self.navigationController?.pushViewController(passingController, animated: true)
    }
    
    func passToAccount(){
        let passingController = self.storyboard?.instantiateViewController(withIdentifier: "trntAccountController") as! trntAccountController
        self.navigationController?.pushViewController(passingController, animated: true)
    }
    
    func passToAuthority(){
        let passingController = self.storyboard?.instantiateViewController(withIdentifier: "trnrAuthorityController") as! trnrAuthorityController
        self.navigationController?.pushViewController(passingController, animated: true)
    }
    
}



extension trnrProfileContoller : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView .dequeueReusableCell(withIdentifier: "trnrCustemCell") as! trnrCustemCell
        cell.detailLbl .text = titleArray[indexPath.row]
        cell.imgvIEW .image = imageArray[indexPath.row]
        
        if indexPath.row == titleArray.count - 1{
            cell.arrowImg .isHidden = true
            cell.lineLbl.isHidden = true
        }else{
            cell.arrowImg .isHidden = false
            cell.lineLbl.isHidden = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0{
            
        }
        else if indexPath .row == 1{
           passToGallry()
        }
        else if indexPath.row == 2{
            passToCard()
        }
        else if indexPath.row == 3{
            passToAuthority()
        }
        else if indexPath.row == 4{
            passToAccount()
        }
    }
}
