//
//  TrnrMyProfilePhotoDeleteVC.swift
//  FITIT
//
//  Created by lokesh chand on 10/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit

class TrnrMyProfilePhotoDeleteVC: UIViewController {
    @IBOutlet weak var myCollectionview: UICollectionView!
    var arrData = [#imageLiteral(resourceName: "0"),#imageLiteral(resourceName: "0"),#imageLiteral(resourceName: "ic_sign_up_approval"),#imageLiteral(resourceName: "blend")]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}
    extension TrnrMyProfilePhotoDeleteVC:UICollectionViewDelegate,UICollectionViewDataSource
    {
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return arrData.count
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
        {
            let cell = collectionView .dequeueReusableCell(withReuseIdentifier: "TrnrPhotoDeleteCollectionCell", for: indexPath) as! TrnrPhotoDeleteCollectionCell
            cell.imgView.image = arrData[indexPath.item]
            cell.imgView.layer.cornerRadius = 20
            cell.imgView.clipsToBounds = true
            cell.imgView.layer.masksToBounds = true
            
            return cell
        }
        
        
        
    }
    
    extension TrnrMyProfilePhotoDeleteVC:UICollectionViewDelegateFlowLayout{
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
        {
            let collectionWidth = myCollectionview.bounds.width
            return CGSize(width: collectionWidth/2 - 18, height: collectionWidth/2 - 25)
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 18
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 25
        }
        
        
    }


