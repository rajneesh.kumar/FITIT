//
//  TrnrPhotoEditCollectionCell.swift
//  FITIT
//
//  Created by lokesh chand on 10/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit

class TrnrPhotoEditCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var tickMark: UIImageView!
    
}
