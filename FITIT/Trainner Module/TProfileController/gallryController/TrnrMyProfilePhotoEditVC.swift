//
//  TrnrMyProfilePhotoEditVC.swift
//  FITIT
//
//  Created by lokesh chand on 10/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit

class TrnrMyProfilePhotoEditVC: UIViewController {
    
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var myCollectionview: UICollectionView!
    
    @IBOutlet weak var backBtn: UIButton! //leftBtn
    @IBOutlet weak var editBtn: UIButton! //rightBtn
    

    var arrData = [#imageLiteral(resourceName: "0"),#imageLiteral(resourceName: "0"),#imageLiteral(resourceName: "ic_sign_up_approval"),#imageLiteral(resourceName: "blend")]
    
    var selectedIndex = [Int]()

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if arrData.count > 0{
            errorView .isHidden = true
            myCollectionview .isHidden = false
        }else{
            errorView .isHidden = false
            myCollectionview .isHidden = true
        }
    }
    
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func addBtnPressed(_ sender: Any) {
        
    }
    
    @IBAction func editBtnPressed(_ sender: Any) {
        
    }
    
}





//MARK: - COLLECTION VIEW METHOD --
extension TrnrMyProfilePhotoEditVC:UICollectionViewDelegate,UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView .dequeueReusableCell(withReuseIdentifier: "TrnrPhotoEditCollectionCell", for: indexPath) as! TrnrPhotoEditCollectionCell
        
        //set the tick image according to the user selection --
         let imageId = indexPath.item + 1
        if selectedIndex.contains(imageId){
            cell.tickMark .image = #imageLiteral(resourceName: "tickMark")
        }else{
            cell.tickMark.image = nil
        }
        
        
        cell.imgView.image = arrData[indexPath.item]
        
        //set the rounede corner on each pic --
        
        cell.imgView.layer.cornerRadius = 25
        cell.imgView.clipsToBounds = true
        cell.imgView.layer.masksToBounds = true
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let imageId = indexPath.item + 1
        print(imageId)
        if selectedIndex .contains(imageId){
            let index = selectedIndex.index(of: imageId)
            selectedIndex.remove(at: index!)
        }else{
            selectedIndex.append(imageId)
        }
        print(selectedIndex)
        self.myCollectionview.reloadData()
    }
    
    
    
 //collection size method --
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let collectionWidth = myCollectionview.bounds.width
        return CGSize(width: collectionWidth/2 - 10, height: collectionWidth/2 - 10)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
     }
    
    }

