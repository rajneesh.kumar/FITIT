//
//  trntAccountController.swift
//  FITIT
//
//  Created by Rajneesh Kumar on 14/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON

class trntAccountController: UIViewController , UITextFieldDelegate {

     @IBOutlet weak var bankTxt: textFieldExt!
     @IBOutlet weak var branchNumberTxt: textFieldExt!
     @IBOutlet weak var accTaxTxt: textFieldExt!
     @IBOutlet weak var nameTxt: textFieldExt!
    
    let alert = alertClass.alertInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
         textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }

    
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func nextBtnPressed(_ sender: Any) {
        if (bankTxt.text?.count)! > 0
        {
            if (branchNumberTxt.text?.count)! > 0
            {
                if (accTaxTxt .text?.count)! > 0
                {
                    if (nameTxt .text?.count)! > 0
                    {
                        self.saveAccountDetails()
                    }
                    else{
                        let msg = "Please enter your name."
                        alert.showAlertWithMessage(Message: msg, ControllerName: self)
                    }
                }
                else{
                    let msg = "Please enter your account number."
                    alert.showAlertWithMessage(Message: msg, ControllerName: self)
                }
            }
            else{
                let msg = "Please enter your branch number."
                alert.showAlertWithMessage(Message: msg, ControllerName: self)
            }
        }
        else{
            let msg = "Please enter your bank name."
            alert.showAlertWithMessage(Message: msg, ControllerName: self)
        }
    }
    
}


extension trntAccountController{
    
    func saveAccountDetails(){
        
        SVProgressHUD.show(withStatus: "Please wait...")
        
        var param = [String : Any]()
        param["user_id"] = UserDefaults.standard .string(forKey: userId)
        param["user_access_token"] = UserDefaults.standard .string(forKey: userToken)
        param["authority_data"] = bankTxt.text
        param["branch_no"] = branchNumberTxt.text
        param["account_tax"] = accTaxTxt.text
        param["name_after"] = nameTxt.text
        
        apiManager.sharedInstance.handleApiCall(controllerName: self, apiUrl: accountSaveApi, parameter: param, requestType: httpmethod.post) { (responseJSON) in
            
            if responseJSON["status"] == "success"{
                SVProgressHUD .showSuccess(withStatus: responseJSON["message"].stringValue)
            }else{
                SVProgressHUD.showError(withStatus: responseJSON["message"].stringValue)
            }
        }
    }
}
