//
//  TMyProfileVC.swift
//  FITIT
//
//  Created by lokesh chand on 10/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit

class TrnrMyProfileVC: UIViewController {
    
    @IBOutlet weak var superview: UIView!
    @IBOutlet var aboutView: UIView!
    @IBOutlet var activityView: UIView!
    @IBOutlet var trainingView: UIView!
    @IBOutlet var detailView: UIView!
    
    @IBOutlet var aboutSlider:UILabel!
    @IBOutlet var activitySlider:UILabel!
    @IBOutlet var trainingSlider:UILabel!
    @IBOutlet var detailSlider:UILabel!
    
    var selectedTab : Int = 13

    override func viewDidLoad() {
        super.viewDidLoad()

        superview.addSubview(detailView)
        detailView.frame = superview.bounds
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        
        //hanlde first time selected tab --
        hanldeSeletedTab(selectedImg: detailSlider, deselectedImg: aboutSlider,deselecttwoImg:activitySlider,deselectThreeImg:trainingSlider)
    }
    
    @IBAction func tabButtonPressed(_ sender: UIButton)
    {
        
        switch sender.tag
        {
        case 10:
            selectedTab = 10
            hanldeSeletedTab(selectedImg: aboutSlider, deselectedImg: activitySlider, deselecttwoImg: trainingSlider, deselectThreeImg: detailSlider)
            activityView.removeFromSuperview()
            trainingView.removeFromSuperview()
            detailView.removeFromSuperview()
            superview.addSubview(aboutView)
            aboutView.frame = superview.bounds
        case 11:
            selectedTab = 11
            hanldeSeletedTab(selectedImg: activitySlider, deselectedImg: aboutSlider, deselecttwoImg: trainingSlider, deselectThreeImg: detailSlider)
            aboutView.removeFromSuperview()
            trainingView.removeFromSuperview()
            detailView.removeFromSuperview()
            superview.addSubview(activityView)
            activityView.frame = superview.bounds
        case 12:
            selectedTab = 12
            hanldeSeletedTab(selectedImg: trainingSlider, deselectedImg: aboutSlider, deselecttwoImg: activitySlider, deselectThreeImg: detailSlider)
            aboutView.removeFromSuperview()
            activityView.removeFromSuperview()
            detailView.removeFromSuperview()
            superview.addSubview(trainingView)
            trainingView.frame = superview.bounds
        case 13:
            selectedTab = 13
            hanldeSeletedTab(selectedImg: detailSlider, deselectedImg: aboutSlider, deselecttwoImg: activitySlider, deselectThreeImg: trainingSlider)
            aboutView.removeFromSuperview()
            activityView.removeFromSuperview()
            trainingView.removeFromSuperview()
            superview.addSubview(detailView)
            detailView.frame = superview.bounds
        default:
            break
        }
    }
    

}
//MARK: - All Custom Function --
extension TrnrMyProfileVC
{
    
    func hanldeSeletedTab(selectedImg:UILabel , deselectedImg:UILabel,deselecttwoImg:UILabel,deselectThreeImg:UILabel)
    {
        selectedImg.isHidden = false
        deselectedImg.isHidden = true
        deselecttwoImg.isHidden = true
        deselectThreeImg.isHidden = true
        
        
    }
}

