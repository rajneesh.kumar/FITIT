//
//  trnrAuthorityController.swift
//  FITIT
//
//  Created by Rajneesh Kumar on 14/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON
import Alamofire

class trnrAuthorityController: UIViewController  , UITextFieldDelegate{
    
    @IBOutlet weak var satckView: UIStackView!
    @IBOutlet weak var stackHeightConst: NSLayoutConstraint!
    @IBOutlet weak var scrollBackHeightConst: NSLayoutConstraint!
    
    
    var authorityArrayObj = [authorityView]()
    var someDict = [String : Any]()
    var authorityData = [[String : String]]()
    
    var scrollViewHight: CGFloat = 400
    var authorityCount = 0
    var tempArry = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addBtnPressed(self)
    }
    
    //texfiled delegate method and datsource ---
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func addAuthorityView(){
        
         authorityCount += 1
        
        //now set the xib ---
        for i in 0 ..< authorityCount{
            
            //add view to the tempArray for check already any view is available or not
            if tempArry .contains(i){
                continue
            }else{
                tempArry.append(i)
                let authorityView = initAuthorityView()
                satckView.addArrangedSubview(authorityView)
                
                //adjust the height of stack view ----
                
                let stkHeight = CGFloat(180 * (i+1))
                let scrollHeight = scrollViewHight + CGFloat(180 * (i+1))
                stackHeightConst .constant = stkHeight
                scrollBackHeightConst.constant = scrollHeight
            }
        }
    }

    
    //init member view ----
    func initAuthorityView() -> UIView{
        
        //init stack view and items --
        let xib = authorityView()
        
        xib.accreditationTxtFiled.delegate = self
        xib.instituteTxtFiled.delegate = self
        xib.startTxtFiled.delegate = self
        xib.startTxtFiled.delegate = self
    
        xib.frame.size = CGSize(width: satckView.frame.size.width, height: 180)
        
        authorityArrayObj .append(xib)
        xib.frame = satckView.bounds
        
        return xib
    }
    
}


//MARK: - ALL BUTTON ACTION HERE--
extension trnrAuthorityController{
    
    @IBAction func submitBtnPresesd(_ sender: Any) {
        
        for i in 0 ..< authorityArrayObj.count{
            let authorityObj = authorityArrayObj[i]
            
            let name = authorityObj.accreditationTxtFiled.text
            let institute = authorityObj.instituteTxtFiled.text
            let startYear = authorityObj.startTxtFiled.text
            let endYear = authorityObj.compliitionTxtFiled.text
            
            let dict = [
                        "name" : name ,
                        "institute" : institute ,
                        "start_year" : startYear ,
                        "end_year" : endYear
                      ]
            authorityData.append(dict as! [String : String])
        }
        
        saveAuthorityData()
    }
    
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addBtnPressed(_ sender: Any) {

        var is_fill : Bool = false
        if authorityArrayObj.count > 0{
            
            for i in 0 ..< authorityArrayObj.count{
                let authorityObj = authorityArrayObj[i]
                
                let name = authorityObj.accreditationTxtFiled.text
                let institute = authorityObj.instituteTxtFiled.text
                let startYear = authorityObj.startTxtFiled.text
                let endYear = authorityObj.compliitionTxtFiled.text
                
                if name == "" || institute == "" || startYear == "" || endYear == ""{
                
                    let msg = "All filed must be filled."
                    alertClass.alertInstance.showAlertWithMessage(Message: msg, ControllerName: self)
                    is_fill = false
                    break
                }else{
                    is_fill = true
                    print("\(i) filed is done") }
            }
            
            if is_fill {
                //add xib into the stack view ---
                addAuthorityView()
            }else{ print("dont need to add view ") }
            
            
        }else{
            //add xib into the stack view ---
            addAuthorityView()
        }

     }
}


//MARK: - API HANLDE --
extension trnrAuthorityController{
    
    func saveAuthorityData(){
     
         SVProgressHUD .show(withStatus: "Please wait...")
        
        //convert array object to the json string ----
        do{
            let data =  try JSONSerialization .data(withJSONObject: authorityData, options: [])
            let authorityStr = String.init(data: data, encoding: String.Encoding.utf8)
            
            
            someDict["user_id"] = UserDefaults.standard .string(forKey: userId)
            someDict["user_access_token"] = UserDefaults.standard .string(forKey: userToken)
            someDict["authority_data"] = authorityStr
        }
            
        catch{ print("some error to parse") }
        
        apiManager.sharedInstance.handleApiCall(controllerName: self, apiUrl: authoritySaveApi, parameter: someDict, requestType: httpmethod.post) { (responseData) in
            
            if responseData["status"].stringValue == "success"{
                SVProgressHUD.showSuccess(withStatus: responseData["message"].stringValue)
            }
            else{
                self.authorityData.removeAll()
                SVProgressHUD.showError(withStatus: responseData["message"].stringValue)
            }
        }
    }
}
