//
//  authorityView.swift
//  FITIT
//
//  Created by Rajneesh Kumar on 14/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit

class authorityView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var accreditationTxtFiled: textFieldExt!
    @IBOutlet weak var instituteTxtFiled: textFieldExt!
    @IBOutlet weak var compliitionTxtFiled: textFieldExt!
    @IBOutlet weak var startTxtFiled: textFieldExt!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initalSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initalSetup()
    }
    
    private func initalSetup(){
        
        Bundle.main.loadNibNamed("authorityView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth , .flexibleHeight]
    }

}
