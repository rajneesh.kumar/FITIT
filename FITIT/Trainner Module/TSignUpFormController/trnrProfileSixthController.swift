//
//  trnrProfileSixthController.swift
//  FITIT
//
//  Created by Rajneesh Kumar on 13/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD

class trnrProfileSixthController: UIViewController , UITextFieldDelegate  , UITextViewDelegate {
    
    @IBOutlet weak var abtTxtView: ExtViewText!
    @IBOutlet weak var fbTxtFiled: UITextField!
    @IBOutlet weak var instaTxtFiled: UITextField!
    
    var keyboardToolbar: UIToolbar?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //set maximum char length in text viuew ---
    }
    
    
    
    func dynamicToobarHandle()
    {
        if keyboardToolbar == nil
        {
            keyboardToolbar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(view.bounds.size.width), height: CGFloat(35)))
            
            let extraspace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            
            let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(resignKeyboard))
            
            keyboardToolbar?.items = [extraspace, doneButton]
            
            keyboardToolbar?.barStyle = .default
        }
        abtTxtView.inputAccessoryView = keyboardToolbar
    }
    
    
    //resgin keyboard ---
    @objc func resignKeyboard(){
        abtTxtView.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField .resignFirstResponder()
        return true
    }
    
    //text view delegate method ---
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = textView.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        let changedText = currentText.replacingCharacters(in: stringRange, with: text)
        
        return changedText.count <= 250
    }
    
    
    func passToContoller(){
        let passingController = self.storyboard?.instantiateViewController(withIdentifier: "trainerNav") as! UINavigationController
        self.present(passingController, animated: true, completion: nil)
    }
}





//MARK: - ALL BUTTON ACTION ---
extension trnrProfileSixthController {
    
    
    @IBAction func backButtonPresesd(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitBtnPressed(_ sender: Any) {
        // saveUserData()
        self.passToContoller()
    }
    
}



//MARK: - HANDLE API  ---
extension trnrProfileSixthController {
    
    func saveUserData(){
        
        SVProgressHUD.show(withStatus: "Please wait ...")
        
        var param = [String : Any]()
        param["user_id"] = UserDefaults.standard .string(forKey: userId)
        param["user_access_token"] = UserDefaults.standard .string(forKey: userToken)
        param["user_fb_link"] = fbTxtFiled.text
        param["user_insta_link"] = instaTxtFiled.text
        param["user_about_me"] = abtTxtView.text
        
        apiManager.sharedInstance.handleApiCall(controllerName: self, apiUrl: traineeSignUp_4_Api, parameter: param, requestType: httpmethod.post) { (responseJSON) in
            
            if responseJSON["status"] == "success"{
                DispatchQueue.main.async {
                    self.passToContoller()
                }
            }
            SVProgressHUD .dismiss()
        }
    }
}


