//
//  addtressView.swift
//  FITIT
//
//  Created by Rajneesh Kumar on 11/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit

class addtressView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var adressTxtFiled: textFieldExt!

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initalSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initalSetup()
    }
    
    private func initalSetup(){
        
        Bundle.main.loadNibNamed("addtressView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth , .flexibleHeight]
    }
}
