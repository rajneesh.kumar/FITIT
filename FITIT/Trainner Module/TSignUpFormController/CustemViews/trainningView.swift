//
//  trainningView.swift
//  FITIT
//
//  Created by Rajneesh Kumar on 12/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit

class trainningView: UIView {

    @IBOutlet var contentView: UIView!
    
    @IBOutlet weak var trainningTypeLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    
    @IBOutlet weak var pricePerHourTxt: textFieldExt!
    @IBOutlet weak var trainDurationTxt: textFieldExt!
    
    @IBOutlet weak var trainningButton: ExtButton!
    @IBOutlet weak var locationBtn: ExtButton!
    
    
    var locationId : String?
    var trainningId : String?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initalSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initalSetup()
    }
    
    private func initalSetup(){
        
        Bundle.main.loadNibNamed("trainningView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth , .flexibleHeight]
    }

}
