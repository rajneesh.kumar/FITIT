//
//  trnrProfileCustemCell.swift
//  FITIT
//
//  Created by Rajneesh Kumar on 20/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit

class trnrProfileCustemCell: UITableViewCell {
    
    
    @IBOutlet weak var detailLbl: UILabel!
    @IBOutlet weak var headingLbl: UILabel!
    @IBOutlet weak var imgView: ExtImg!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
