//
//  trnrProfileFifthController.swift
//  FITIT
//
//  Created by Rajneesh Kumar on 20/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD




struct personalData {
     var training_id: String?
     var location_id_fk: String?
     var training_option: String?
     var training_duration: String?
     var training_wages: String?
     var workout_id: String?
     var workout_slug: String?
     var training_type: String?
     var workout_icon: String?
}

struct doubleData {
    
    var training_id: String?
    var location_id_fk: String?
    var training_option: String?
    var training_duration: String?
    var training_wages: String?
    var workout_id: String?
    var workout_slug: String?
    var training_type: String?
    var workout_icon: String?
}

struct collectionData {
    var training_id: String?
    var location_id_fk: String?
    var training_option: String?
    var training_duration: String?
    var training_wages: String?
    var workout_id: String?
    var workout_slug: String?
    var training_type: String?
    var workout_icon: String?
    var participants : String?
}



class trnrProfileFifthController: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    
    var tapGesture = UITapGestureRecognizer()
    
    var selectedId : Int = 12
    var batchId : String?
    
    var personalDataArray = [personalData]()
    var doubleDataArray = [doubleData]()
    var collectionDataArray = [collectionData]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initTapGesture()
        getTrainnerlist()

    }
    
    
    //add tap geture on all img view --
    func initTapGesture(){
        
        for i in 10 ..< 13{
            let view = self.view.viewWithTag(i) as! ExtImg
            if i == 12{
                view.layer .borderWidth = 3.0
                view.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.3607843137, blue: 1, alpha: 1)
            }
            gestureMethod(view: view)
        }
    }
    
    
//PASS TO NEXT CONTROLLER --
    func passToNextController(){
        let passingController = self.storyboard?.instantiateViewController(withIdentifier: "trnrProfileSixthController") as! trnrProfileSixthController
        self.present(passingController, animated: true, completion: nil)
    }
    
//SKIP TO MAIN CONTROLLER --
    func skipToContoller(){
        let passingController = self.storyboard?.instantiateViewController(withIdentifier: "trainerNav") as! UINavigationController
        self.present(passingController, animated: true, completion: nil)
    }
    

    @IBAction func backBtnPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func nextBtnPressed(_ sender: Any) {
        passToNextController()
    }
    
    @IBAction func skipBtnPresesd(_ sender: Any) {
       skipToContoller()
    }
}



//MARK: - TAP GETSURE HANLDE ---
extension trnrProfileFifthController {
    
    func gestureMethod(view : UIImageView){
        
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        view.isUserInteractionEnabled = true
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapView))
        view.addGestureRecognizer(tapGesture)
    }
    
    
    @objc func handleTapView(_ sender:UITapGestureRecognizer){
        
        let tag = sender.view?.tag
        selectedId = tag!
        
        for i in 10 ..< 13{
            if tag == i{
                let currentImg = self.view.viewWithTag(i) as! ExtImg
                currentImg.layer.borderWidth = 3.0
                currentImg.layer.borderColor =  #colorLiteral(red: 0.4392156863, green: 0.3607843137, blue: 1, alpha: 1)
                
            }else{
                let currentImg = self.view.viewWithTag(i) as! ExtImg
                currentImg.layer.borderWidth = 0.0
                currentImg.layer.borderColor = UIColor.clear.cgColor
            }
        }
        
        DispatchQueue.main.async {
            self.tblView.reloadData()
        }
    }
}




//MARK: - TABLE VIEW DELGATE AND DATASOURCE ----
extension trnrProfileFifthController  : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
         return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedId == 12{
            return personalDataArray.count
        }else if selectedId == 11{
            return doubleDataArray.count
        }else{
            return collectionDataArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView .dequeueReusableCell(withIdentifier: "trnrProfileCustemCell") as! trnrProfileCustemCell
        
        if selectedId == 12{
            let objData = personalDataArray[indexPath.row]
            cell.headingLbl .text = objData.training_type
            cell.detailLbl .text = objData.training_wages! + " " + objData.training_type!
            
            //get the image --
            if let ImgStr =  objData.workout_icon{
                let imgUrl = URL(string:ImgStr)
                cell.imgView.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "placeholder.png"))
            }
        }
        else if selectedId == 11{
                let objData = doubleDataArray[indexPath.row]
                cell.headingLbl .text = objData.training_type
                cell.detailLbl .text = objData.training_wages! + " " + objData.training_type!
                
                //get the image --
                if let ImgStr =  objData.workout_icon{
                    let imgUrl = URL(string:ImgStr)
                    cell.imgView.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "placeholder.png"))
            }
        }else{
            let objData = collectionDataArray[indexPath.row]
            cell.headingLbl .text = objData.training_type
            cell.detailLbl .text = objData.training_wages! + " " + objData.training_type!
            
            //get the image --
            if let ImgStr =  objData.workout_icon{
                let imgUrl = URL(string:ImgStr)
                cell.imgView.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "placeholder.png"))
            }
            
        }
        return cell
    }
}






//MARK: - ALL API HANDLE HERE ---
extension trnrProfileFifthController{
    
    func getTrainnerlist(){
        
        var someDict = [String : Any]()
        someDict["user_id"] = UserDefaults.standard.string(forKey: userId)
        someDict["user_access_token"] = UserDefaults.standard.string(forKey: userToken)
        someDict["batch_id"] = batchId
        
        apiManager.sharedInstance.handleApiCall(controllerName: self, apiUrl: trainerGetList, parameter: someDict, requestType: httpmethod.post) { (apiData) in
            
            if apiData["status"].stringValue == "success"{
                
                let responseArray = apiData["data"].arrayValue
                for i in 0 ..< responseArray.count{
                    
                    if responseArray[i]["type"] == "personal"{
                        var personalArray = responseArray[i]["data"].arrayValue
                        
                        for j in 0 ..< personalArray.count{
                            let training_id = personalArray[j]["training_id"].stringValue
                            let location_id_fk = personalArray[j]["location_id_fk"].stringValue
                            let training_option = personalArray[j]["training_option"].stringValue
                            let training_duration = personalArray[j]["training_duration"].stringValue
                            let training_wages = personalArray[j]["training_wages"].stringValue
                            let workout_id = personalArray[j]["workout_id"].stringValue
                            let workout_slug = personalArray[j]["workout_slug"].stringValue
                            let training_type = personalArray[j]["training_type"].stringValue
                            let workout_icon = personalArray[j]["workout_icon"].stringValue
                            
                            let personalObj = personalData(training_id: training_id, location_id_fk: location_id_fk, training_option: training_option, training_duration: training_duration, training_wages: training_wages, workout_id: workout_id, workout_slug: workout_slug, training_type: training_type, workout_icon: workout_icon)
                            
                            self.personalDataArray.append(personalObj)
                        }
                    }else if responseArray[i]["type"] == "double"{
                        var doubleArray = responseArray[i]["data"].arrayValue
                        
                        for j in 0 ..< doubleArray.count{
                            let training_id = doubleArray[j]["training_id"].stringValue
                            let location_id_fk = doubleArray[j]["location_id_fk"].stringValue
                            let training_option = doubleArray[j]["training_option"].stringValue
                            let training_duration = doubleArray[j]["training_duration"].stringValue
                            let training_wages = doubleArray[j]["training_wages"].stringValue
                            let workout_id = doubleArray[j]["workout_id"].stringValue
                            let workout_slug = doubleArray[j]["workout_slug"].stringValue
                            let training_type = doubleArray[j]["training_type"].stringValue
                            let workout_icon = doubleArray[j]["workout_icon"].stringValue
                            
                            let doubleObj = doubleData(training_id: training_id, location_id_fk: location_id_fk, training_option: training_option, training_duration: training_duration, training_wages: training_wages, workout_id: workout_id, workout_slug: workout_slug, training_type: training_type, workout_icon: workout_icon)
                            
                            self.doubleDataArray.append(doubleObj)
                        }
                    }else{
                        var collectionArray = responseArray[i]["data"].arrayValue
                        
                        for j in 0 ..< collectionArray.count{
                            let training_id = collectionArray[j]["training_id"].stringValue
                            let location_id_fk = collectionArray[j]["location_id_fk"].stringValue
                            let training_option = collectionArray[j]["training_option"].stringValue
                            let training_duration = collectionArray[j]["training_duration"].stringValue
                            let training_wages = collectionArray[j]["training_wages"].stringValue
                            let workout_id = collectionArray[j]["workout_id"].stringValue
                            let workout_slug = collectionArray[j]["workout_slug"].stringValue
                            let training_type = collectionArray[j]["training_type"].stringValue
                            let workout_icon = collectionArray[j]["workout_icon"].stringValue
                            let participants = collectionArray[j]["participants"].stringValue
                            
                            let collectionObj = collectionData(training_id: training_id, location_id_fk: location_id_fk, training_option: training_option, training_duration: training_duration, training_wages: training_wages, workout_id: workout_id, workout_slug: workout_slug, training_type: training_type, workout_icon: workout_icon, participants: participants)
                            
                            self.collectionDataArray.append(collectionObj)
                        }
                    }
                }
            }
            
            DispatchQueue.main.async {
                self.tblView.reloadData()
            }
            SVProgressHUD.dismiss()
        }
        
    }
}
