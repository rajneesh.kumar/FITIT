//
//  TrnrProfileThreeVC.swift
//  FITIT
//
//  Created by lokesh chand on 08/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON



struct coordinateData {
    var coordinate_id : String?
    var coordinate_name : String?
}


struct positionData {
    var position_id : String?
    var position_name : String?
    var position_slug : String?
    var position_icon : String?
}

class custemCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var detailLbl: UILabel!
    @IBOutlet weak var lineLbl: UILabel!
    @IBOutlet weak var imageView: ExtImg!
    
}




class TrnrProfileThreeVC: UIViewController , UITextFieldDelegate {

    @IBOutlet weak var addressTxtFiled: textFieldExt!
    @IBOutlet weak var coordinateLbl: UILabel!
    
    @IBOutlet weak var addSliderView: UIView!
    @IBOutlet weak var stackView: UIStackView!
    
    @IBOutlet var sliderView: UIView!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var addButton: ExtButton!
    
    
    @IBOutlet weak var sliderHeightConst: NSLayoutConstraint!
    @IBOutlet weak var stackHeightConst: NSLayoutConstraint!
    @IBOutlet weak var mainViewHightConst: NSLayoutConstraint!
    
    @IBOutlet weak var pickerBckView: UIView!
    @IBOutlet weak var pickerView: UIPickerView!
    
    
    var tapGesture = UITapGestureRecognizer()
   
    var cordList =  [coordinateData]()
    var positionListData = [positionData]()
    
    
    var xibObj = [addtressView]()
    var addressDict = [Int : [addtressView]]()
    var sliderDict = [Int : Float]()
    
    
    var gymObject = [[String : Any]]()
    var adress = [String]() //storing address
    var slideraddress = [String]()
    var dataDict = [String : Any]()
    
    var tempArry = [Int]()
    var cordinteId : String = ""
    
    var sliderValue : Float = 0.0
    var addressCount : Int = 0
   
    var scrollViewHight : CGFloat = 680
    var stackViewHeight : CGFloat = 0
    var sliderHeight : CGFloat = 0
    

    var is_sliderSelected : Bool = false
    var selectedItem : Int?

    

    override func viewDidLoad(){
        super.viewDidLoad()
        self.addButton .isHidden = true

        //get all the coordinate data && get all the position list data ---
        getExamptList()
        getWorkoutPositionList()
    }
    
    
//textfiled delegate ----
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

//PASS TO NEXT CONTROLLER --
    func passToNextController(activityId : String){
        let passingController = self.storyboard?.instantiateViewController(withIdentifier: "trnrProfileFourController") as! trnrProfileFourController
        self.present(passingController, animated: true, completion: nil)
    }
    
//SKIP TO MAIN CONTROLLER --
    func skipToContoller(){
        let passingController = self.storyboard?.instantiateViewController(withIdentifier: "trainerNav") as! UINavigationController
        self.present(passingController, animated: true, completion: nil)
    }
    
}


//MARK: - HANDLE ADDRESS VIEW ADDED && SLIDER VIEW - -
extension TrnrProfileThreeVC {
    
    
    
    //add sliderView ---
    func initSliderView(is_added : Bool){
        
        if is_added{
            
            //add key to slider dict ---
            sliderDict[selectedItem!] = sliderValue
            
            
            //add the slider to the slider super view --
            sliderView.removeFromSuperview()
            addSliderView.addSubview(sliderView)
            
            sliderHeightConst .constant = 110.0
            sliderView.frame = addSliderView.bounds
            
            slider.value = sliderValue
            
            //height manage for scroll view after add slider view ---
            sliderHeight = 110
            mainViewHightConst .constant = scrollViewHight + sliderHeight + stackViewHeight
        }
        else{
            sliderView.removeFromSuperview()
            sliderHeightConst .constant = 0.0
            sliderHeight = 0
            //let totalHeight = scrollViewHight + slider
            mainViewHightConst .constant = scrollViewHight + sliderHeight + stackViewHeight
        }
    }
    
    
    //ADD ADDRESS VIEW  ----
    func initAddressView() -> UIView{
        
        //init stack view and items --
        let xib = addtressView()
        xib.adressTxtFiled.delegate = self
        
        xib.frame = stackView.bounds
        xib.frame.size = CGSize(width: stackView.frame.size.width, height: 45.0)
        
        //make key for save addressView obj correspondence this key
        let key = selectedItem!
        
        //but before adding the key we have to make array for xib -
        xibObj .append(xib)
        
        //but we have to also check that this key is available or not before inserting into dict--
        
        if let keyExit = addressDict[key]{
            var previousArray = keyExit
            previousArray .append(xib)
            addressDict[key] = previousArray
            
            xibObj.removeAll()
            
        }else{
            //now add the value for upper key that we made --
            addressDict[key] = xibObj
            xibObj.removeAll()
        }
        return xib
    }
    
    func preTxtIsFilled() -> Bool{
        
        if let keyExit = addressDict[selectedItem!]{
           for i in 0 ..< keyExit.count
           {
                 let obj = keyExit[i]
                 if (obj.adressTxtFiled.text?.count)! > 0 { continue }
                 else{ return false }
            }
            return true
        }
        else{ return true }
    }
    
    func setDataObj(){
        
    //get all the address and bind the data --
        let keyCollection = addressDict.keys
        let keyArray = Array(keyCollection)
        var isKeyAdded : Bool = false
        
        let sliderKey = sliderDict.keys
        let sliderKeyArry = Array(sliderKey)
    
        
        if is_sliderSelected {
            let key = sliderKeyArry[0]
            let position = positionListData[key]
            dataDict["workout_id"] = position.position_id
            dataDict["gym_address"] = slideraddress
            dataDict["service_distance"] = "\(sliderValue)"
            
            //convert in to json object ---
            
            do{
                let data =  try JSONSerialization .data(withJSONObject: dataDict, options: [])
                do{
                    let addressStr = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    gymObject.append(addressStr as! [String : Any])
                }catch { }
                
            }catch { }
        }
        
        for i in 0 ..< keyArray.count{
            
            isKeyAdded = false
            let key = keyArray[i]
            let value = addressDict[key]
            
            
            if let count = value?.count
            {
                adress.removeAll()
                let position = positionListData[key]
                
                for i in 0 ..< count{
                    
                    let obj = value![i]
                    let adrs = obj.adressTxtFiled.text
                    
                    if (adrs?.count)! > 0 && isKeyAdded == false{
                        dataDict["workout_id"] = position.position_id
                        dataDict["service_distance"] = ""
                    }else{
                        isKeyAdded = true
                        break
                    }
                    adress.append(adrs!)
                }
                dataDict["gym_address"] = adress
                
                
            //convert all the address dict to json object ---
                if adress.count > 0{
                    do{
                        let data =  try JSONSerialization .data(withJSONObject: dataDict, options: [])
                        do{
                            let addressStr = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                            gymObject.append(addressStr as! [String : Any])
                        }catch { }
                        
                    }catch { }
                }
            }
            else {
                //here we add object of map radius ---
                
            }
        }
        
        
       //if there is any object , then api is called --
        if gymObject.count > 0 || is_sliderSelected == true{
            saveUserData()
        }else{
            let msg = "Before proceeding select any option or fill the range of distance."
            alertClass.alertInstance.showAlertWithMessage(Message: msg, ControllerName: self)
        }
    }

}







//MARK: - ALL BUTTON ACTION DEFINED ---
extension TrnrProfileThreeVC {
    
    @IBAction func doneBtnPressed(_ sender: Any) {
        self.pickerBckView.isHidden = true
    }
    
    
    @IBAction func addAddressView(_ sender: Any) {
    //before add new text filed we have to check user filled previous text field or not ---
        
        let isTextAdded = preTxtIsFilled()
        
        if isTextAdded{
            
            addressCount += 1
            
            for i in 0 ..< addressCount{
                
                //add view to the tempArray for check already any view is available or not
                if tempArry .contains(i){
                    continue
                }else{
                    tempArry.append(i)
                    let adrsView = initAddressView()
                    stackView.addArrangedSubview(adrsView)
                    
                    //adjust the height of stack view ----
                    
                    let stkHeight = CGFloat(45 * (i+1))
                    let scrollHeight = scrollViewHight + CGFloat(45 * (i+1))
                    
                    stackHeightConst .constant = stkHeight
                    stackViewHeight = stkHeight
                    
                    scrollViewHight = scrollHeight
                    mainViewHightConst .constant = scrollViewHight + sliderHeight + stackViewHeight
                }
            }
            
        }
    }
    
    
    @IBAction func saveButtonPressed(_ sender: Any) {
    }
    
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        let value = sender.value
        self.sliderValue = value
    }
    
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func coordinateBtnPressed(_ sender: Any) {
        self.pickerBckView .isHidden = false
    }
    
    @IBAction func nextBtnPressed(_ sender: Any) {
    
        let keyCollection = addressDict.keys
        let keyArray = Array(keyCollection)
        
        if is_sliderSelected || keyArray.count > 0{
             setDataObj()
        }
        else{
           let msg = "Please select any workout position."
           alertClass.alertInstance.showAlertWithMessage(Message: msg, ControllerName: self)
        }
    }
    
    
    func removeTextFiled(currentIndex : Int){
        
        //get the key for find value --
        let keyCollection = addressDict.keys
        let keyArray = Array(keyCollection)
        
        for i in 0 ..< keyArray.count{
            let key = keyArray[i]
            if key != currentIndex{
                if let addrsArray = addressDict[key]{
                    
                    for j in 0 ..< addrsArray.count{
                        let obj = addrsArray[j]
                        obj.removeFromSuperview()
                    }
                 }
             }
        }
    }
    
    
    func hanldeTextFiled(currentIndex : Int){
        
        if let addresArray = addressDict[currentIndex]
        {
            for j in 0 ..< addresArray.count{
                let obj = addresArray[j]
                stackView .addArrangedSubview(obj)
                addressCount += 1
                tempArry .append(j)
            }
        }else{
            addAddressView(self)
        }
    }
    
    @IBAction func skipBtnPressed(_ sender: Any) {
        skipToContoller()
    }
}







//MARK: - PICKER FUNCTIONALITY DEFINED ---
extension TrnrProfileThreeVC : UIPickerViewDelegate , UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return cordList.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        let cordRow = cordList[row]
        return cordRow.coordinate_name
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let cordRow = cordList[row]
        self.coordinateLbl .text = cordRow.coordinate_name
        cordinteId = cordRow.coordinate_id!
    }
}








//MARK: - COLLECTION DELEGATE AND METHOD --
extension TrnrProfileThreeVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return positionListData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        let cell = collectionView .dequeueReusableCell(withReuseIdentifier: "custemCollectionCell", for: indexPath) as! custemCollectionCell
        
        //manage line between each row --
        if indexPath.item == positionListData.count - 1 || indexPath.item == 2{
            cell.lineLbl .isHidden = true
        }else{
            cell.lineLbl.isHidden = false
        }
        
        //manage the border while selecting item --
        let userPosition = positionListData[indexPath.item]
        cell.detailLbl .text = userPosition.position_name
        
        if selectedItem == indexPath.item{
            cell.imageView .layer .borderWidth = 3.0
            cell.imageView.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.3607843137, blue: 1, alpha: 1)
        }
        else{
            cell.imageView .layer .borderWidth = 0.0
            cell.imageView.layer.borderColor = UIColor.clear.cgColor
        }
    
        //get the image --
        if let posImgStr =  userPosition.position_icon{
            let imgUrl = URL(string:posImgStr)
            cell.imageView.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "placeholder.png"))
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            selectedItem = indexPath.item
        
            let positionItem = positionListData[indexPath.item]
            //reset all the values --
            tempArry.removeAll()
            addressCount = 0
        
        if positionItem .position_id == "6"{
            self.is_sliderSelected = true
            self.addButton .isHidden = true
            removeTextFiled(currentIndex: indexPath.item)
            initSliderView(is_added: true)
        }else{
            self.addButton .isHidden = false
            initSliderView(is_added: false)
            
            removeTextFiled(currentIndex: indexPath.item)
            hanldeTextFiled(currentIndex: indexPath.item)
            
        }
        self.collectionView.reloadData()
  
    }
    
    
    //collection size class method ---
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        let cellSize = collectionView.bounds.width / 3  - 5
        return CGSize(width: cellSize, height: cellSize - 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}









//MARK: - API CALL FUNCTIONALITY DEFINED ---
extension TrnrProfileThreeVC {
    
    
//save the user data ----
    func saveUserData(){
    
        SVProgressHUD .show(withStatus: "Please wait...")
        
        var param = [String : Any]()
        param["user_id"] = UserDefaults.standard.string(forKey: userId)
        param["user_access_token"] = UserDefaults.standard.string(forKey: userToken)
        param["coordinator_id"] = cordinteId
        param["activity_address"] = addressTxtFiled.text
        param["activity_lat"] = ""
        param["activity_long"] = ""
        
        
        //convert array object to the json string ----
        do{
            let data =  try JSONSerialization .data(withJSONObject: gymObject, options: [])
            let addressStr = String.init(data: data, encoding: String.Encoding.utf8)
            param["workout_position"] = addressStr
        } catch { }
        
        
        
        print(param)
        
        apiManager.sharedInstance.handleApiCall(controllerName: self, apiUrl: trainerSignUp_2_Api, parameter: param, requestType: httpmethod.post) { (apiData) in
            
            if apiData["status"].stringValue == "success"{
                SVProgressHUD .showSuccess(withStatus: apiData["message"].stringValue)
                
                DispatchQueue.main.async {
                    print(apiData["data"]["activity_id"].stringValue)
                    self.passToNextController(activityId: apiData["data"]["activity_id"].stringValue)
                }
            }
            else{
                SVProgressHUD .showError(withStatus: apiData["message"].stringValue)
            }
        }
    }
    
    
    
//get exampt list ---
    func getExamptList(){
        SVProgressHUD.show(withStatus: "Please wait...")
        
        var param = [String : Any]()
        param["user_id"] = UserDefaults.standard.string(forKey: userId)
        param["user_access_token"] = UserDefaults.standard.string(forKey: userToken)
        
        apiManager.sharedInstance.handleApiCall(controllerName: self, apiUrl: coordinateListApi, parameter: param, requestType: httpmethod.post) { (responseJSON) in
            
            if responseJSON["status"] == "success"{
                
                let idArray = responseJSON["data"].arrayValue
                
                for i in 0 ..< idArray.count{
                    let id = idArray[i]["coordinator_id"].stringValue
                    let name = idArray[i]["coordinator_name"].stringValue
                    let crdData = coordinateData(coordinate_id: id, coordinate_name: name)
                    self.cordList.append(crdData)
                }
            }
            DispatchQueue.main.async {
                self.pickerView.reloadAllComponents()
            }
            SVProgressHUD .dismiss()
        }
    }
    
    
    //get exampt list ---
    func getWorkoutPositionList(){
        SVProgressHUD.show(withStatus: "Please wait...")
        
        var param = [String : Any]()
        param["user_id"] = UserDefaults.standard.string(forKey: userId)
        param["user_access_token"] = UserDefaults.standard.string(forKey: userToken)
        
        apiManager.sharedInstance.handleApiCall(controllerName: self, apiUrl: getWorkoutPositionlist, parameter: param, requestType: httpmethod.post) { (responseJSON) in
            
            if responseJSON["status"] == "success"{
                
                let dataArray = responseJSON["data"].arrayValue
                
                for i in 0 ..< dataArray.count{
                    let id = dataArray[i]["workout_id"].stringValue
                    let name = dataArray[i]["workout_name"].stringValue
                    let slug = dataArray[i]["workout_slug"].stringValue
                    let imgUrl = dataArray[i]["workout_icon"].stringValue
                    
                    let posData = positionData(position_id: id, position_name: name, position_slug: slug, position_icon: imgUrl)
                    self.positionListData.append(posData)
                   
                }
            }
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            SVProgressHUD .dismiss()
        }
    }
    
}




