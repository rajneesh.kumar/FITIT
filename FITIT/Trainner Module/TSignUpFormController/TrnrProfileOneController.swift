//
//  TrnrProfileOneController.swift
//  FITIT
//
//  Created by Rajneesh Kumar on 11/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD



struct examptData {
    var exempt_id : String?
    var exempt_name : String?
}





class TrnrProfileOneController: UIViewController , UITextFieldDelegate {
    
    @IBOutlet weak var profileImgview: UIImageView!
    @IBOutlet weak var firstNameTxt: textFieldExt!
    @IBOutlet weak var lastNameTxt: textFieldExt!
    @IBOutlet weak var birthView: ExtView!
    @IBOutlet weak var dobLbl: UILabel!
    @IBOutlet weak var segmentController: UISegmentedControl!
    
    
    @IBOutlet weak var compnyNameTxt: textFieldExt!
    @IBOutlet weak var exmptLbl: UILabel!
    @IBOutlet weak var numberIdTxt: textFieldExt!
    @IBOutlet weak var cityTxt: textFieldExt!
    @IBOutlet weak var addressTxt: textFieldExt!
    @IBOutlet weak var phoneTxt: textFieldExt!
    
    
    @IBOutlet weak var pickerBackView: UIView!
    @IBOutlet weak var pickerAddView: UIView!
    
    
    @IBOutlet var datePickerView: UIDatePicker!
    @IBOutlet var pickerView: UIPickerView!
    
    var seletedDate = ""
    var userGender = "f"
    var userImg : UIImage?
    
    var exemptList =  [examptData]()
    var exemptId : String = ""
    
    var imagePicker = UIImagePickerController()
    var tapGesture = UITapGestureRecognizer()
    
    var keyboardToolbar: UIToolbar?
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addTapGesture(view: birthView)
        addImgTapGesture(view: profileImgview)
        pickerBackView.isHidden = true
        
//add the tolbar to the keyboard ---
        dynamicToobarHandle()
        
//get examplist from master data ---
        getExamptList()
        
    }
    

//add toolbar to keyboard --
    func dynamicToobarHandle()
    {
        if keyboardToolbar == nil
        {
            keyboardToolbar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(view.bounds.size.width), height: CGFloat(35)))
            
            let extraspace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            
            let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(resignKeyboard))
            
            keyboardToolbar?.items = [extraspace, doneButton]
            
            keyboardToolbar?.barStyle = .default
        }
        phoneTxt.inputAccessoryView = keyboardToolbar
    }
    
    
    
//resgin keyboard ---
    @objc func resignKeyboard(){
        phoneTxt.resignFirstResponder()
    }
    
    
//textfiled Delegate and method ---
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
//PASS TO NEXT CONTROLLER --
    func passToNextController(){
        let passingController = self.storyboard?.instantiateViewController(withIdentifier: "TrnrProfileThreeVC") as! TrnrProfileThreeVC
        self.present(passingController, animated: true, completion: nil)
    }
    
 //SKIP TO MAIN CONTROLLER --
    func skipToContoller(){
        let passingController = self.storyboard?.instantiateViewController(withIdentifier: "trainerNav") as! UINavigationController
        self.present(passingController, animated: true, completion: nil)
    }
    
}





//MARK: - ALL BUTTON ACTION DEFINED ---
extension TrnrProfileOneController {
    
    //ACTION METHOD FOR THE  DATE PICKER --
    @IBAction func dateValueChanged(_ sender: UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        seletedDate =  dateFormatter.string(from: sender.date)
        dobLbl .text = seletedDate
    }
    
    
    @IBAction func doneBtnPressed(_ sender: UIButton) {
        self.pickerBackView .isHidden = true
        pickerView.removeFromSuperview()
        datePickerView.removeFromSuperview()
    }
    
    
    @IBAction func exmeptListPressed(_ sender: Any) {
        
        datePickerView.removeFromSuperview()
        pickerBackView .isHidden = false
        pickerAddView .addSubview(pickerView)
        pickerView .frame = pickerAddView.bounds
    }
    
    
    @IBAction func segmentValueChnaged(_ sender: UISegmentedControl) {
        if segmentController.selectedSegmentIndex == 0{
            userGender = "f"
        }else{
            userGender = "m"
        }
    }
    
    
    @IBAction func nextBtnPressed(_ sender: Any) {
//        if (firstNameTxt.text?.count)! > 0{
//            if (lastNameTxt.text?.count)! > 0{
//                if seletedDate != ""{
//                    if (compnyNameTxt.text?.count)! > 0{
//                        if exemptId != ""{
//                            if (numberIdTxt .text?.count)! > 0{
//                                if (cityTxt .text?.count)! > 0{
//                                    if (addressTxt .text?.count)! > 0{
//                                        if (phoneTxt .text?.count)! > 0{
//
//                                            handleUserData()
//                                        }else{
//                                            let msg = "Please enter your mobile number."
//                                            alertClass.alertInstance.showAlertWithMessage(Message: msg, ControllerName: self)
//                                        }
//                                    }else{
//                                        let msg = "Please enter your address."
//                                        alertClass.alertInstance.showAlertWithMessage(Message: msg, ControllerName: self)
//                                    }
//                                }else{
//                                    let msg = "Please enter your city."
//                                    alertClass.alertInstance.showAlertWithMessage(Message: msg, ControllerName: self)
//                                }
//                            }else{
//                                let msg = "Please enter your numberId."
//                                alertClass.alertInstance.showAlertWithMessage(Message: msg, ControllerName: self)
//                            }
//                        }else{
//                            let msg = "Please select any exempt id."
//                            alertClass.alertInstance.showAlertWithMessage(Message: msg, ControllerName: self)
//                        }
//                    }else{
//                        let msg = "Please enter your compny name."
//                        alertClass.alertInstance.showAlertWithMessage(Message: msg, ControllerName: self)
//                    }
//                }else{
//                    let msg = "Please select your date of birth."
//                    alertClass.alertInstance.showAlertWithMessage(Message: msg, ControllerName: self)
//                }
//            }else{
//                let msg = "Please enter your last name."
//                alertClass.alertInstance.showAlertWithMessage(Message: msg, ControllerName: self)
//            }
//        }else{
//            let msg = "Please enter your first name."
//            alertClass.alertInstance.showAlertWithMessage(Message: msg, ControllerName: self)
//        }
         //handleUserData()
        passToNextController()
    }
    
    
    @IBAction func skipBtnPressed(_ sender: Any) {
        skipToContoller()
    }
    
}





//MARK: - PICKER FUNCTIONALITY DEFINED ---
extension TrnrProfileOneController : UIPickerViewDelegate , UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return exemptList.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        let exemptRow = exemptList[row]
        return exemptRow.exempt_name
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let exemptRow = exemptList[row]
        self.exmptLbl .text = exemptRow.exempt_name
        exemptId = exemptRow.exempt_id!
    }
}





//MARK: - TAP FUNCTIONALITY DEFINED ---
extension TrnrProfileOneController {
    
//THIS TAP GESTURE ONLY FOR THE DATE VIEW
    func addTapGesture(view : UIView)
    {
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        view.isUserInteractionEnabled = true
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapView(_:)))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func handleTapView(_ sender:UITapGestureRecognizer){
        
        pickerView.removeFromSuperview()
        pickerBackView .isHidden = false
        pickerAddView .addSubview(datePickerView)
        datePickerView .frame = pickerAddView.bounds
    }
    
    
    
//THIS TAP GESTURE ONLY FOR THE DATE VIEW
    func addImgTapGesture(view : UIImageView)
    {
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        view.isUserInteractionEnabled = true
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleImgTapView(_:)))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func handleImgTapView(_ sender:UITapGestureRecognizer){
        self.handleImageSelection()
    }
}




//MARK: - IMAGE SELECTION FUNCTIONALITY DEFINED ---
extension TrnrProfileOneController  : UIImagePickerControllerDelegate , UINavigationControllerDelegate{
    
    func handleImageSelection(){
        self.imagePicker.sourceType = .photoLibrary
        self.imagePicker.delegate = self
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]){
        
        userImg = info["UIImagePickerControllerOriginalImage"] as? UIImage
        profileImgview .image = userImg
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        self.dismiss(animated: true, completion: nil)
    }
    
}





//MARK: - API CALL FUNCTIONALITY DEFINED ---
extension TrnrProfileOneController {
    
//get exampt list ---
    func getExamptList(){
        SVProgressHUD.show(withStatus: "Please wait...")
        
        var param = [String : Any]()
        param["user_id"] = UserDefaults.standard .string(forKey: userId)
        param["user_access_token"] = UserDefaults.standard .string(forKey: userToken)
        
        apiManager.sharedInstance.handleApiCall(controllerName: self, apiUrl: examptlistApi, parameter: param, requestType: .post) { (responseJSON) in
            
            if responseJSON["status"] == "success"{
                
                let idArray = responseJSON["data"].arrayValue
                
                for i in 0 ..< idArray.count{
                    let id = idArray[i]["exempt_id"].stringValue
                    let name = idArray[i]["exempt_name"].stringValue
                    let exmData = examptData(exempt_id: id, exempt_name: name)
                    self.exemptList.append(exmData)
                }
            }
            DispatchQueue.main.async {
                self.pickerView.reloadAllComponents()
            }
            SVProgressHUD .dismiss()
        }
    }
    
    
 //save user data ----
    func handleUserData(){
        
        SVProgressHUD.show(withStatus: "Please wait...")
        var param = [String : Any]()
        param["user_id"] = UserDefaults.standard .string(forKey: userId)
        param["user_access_token"] = UserDefaults.standard .string(forKey: userToken)
        param["user_fname"] = firstNameTxt.text
        param["user_lname"] = lastNameTxt.text
        param["user_dob"] = seletedDate
        param["user_phone"] = phoneTxt.text
        param["user_gender"] = userGender
        param["company_name"] = compnyNameTxt.text
        param["exempt_id"] = exemptId
        param["company_id_no"] = numberIdTxt.text
        param["company_address"] = addressTxt.text
        param["company_phone"] = phoneTxt.text
        param["company_city"] = cityTxt.text
        param["company_lat"] = ""
        param["company_long"] = ""
        
        let headers: HTTPHeaders = [ "Content-type": "multipart/form-data" ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in param{
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            //set the user image ---
            if let selctedImg = self.userImg{
                if let imgData = UIImageJPEGRepresentation(selctedImg, 0.6){
                    multipartFormData.append(imgData, withName: "user_profile_pic", fileName: "userProfile.jpg", mimeType: "image")
                }
            }
            else { print("imgae not found") }
            
            
        }, usingThreshold: UInt64.init(), to: trainerSignUpApi, method: .post, headers: headers) { (result) in
            
            switch result
            {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    
                    //print(JSON(response.data))
                    SVProgressHUD .dismiss()
                    DispatchQueue.main.async {
                        self.passToNextController()
                    }
                    
                    if let err = response.error{
                        print(err)
                        return
                    }
                }
                
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                SVProgressHUD.dismiss()
            }
        }
    }
}

