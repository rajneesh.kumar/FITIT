//
//  trnrProfileFourController.swift
//  FITIT
//
//  Created by Rajneesh Kumar on 12/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD


struct trainningTypeData {
    var training_id : String?
    var training_type : String?
    var training_icon : String?
}


struct locationData {
    var location_id : String?
    var location_name : String?
}



class trnrProfileFourController: UIViewController , UITextFieldDelegate  {
    
    @IBOutlet weak var collectIMgView: ExtImg!
    @IBOutlet weak var doubleImgview: ExtImg!
    @IBOutlet weak var singleImgView: ExtImg!
    
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var pickerBackView: UIView!
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var stackHeightConst: NSLayoutConstraint!
    @IBOutlet weak var mainViewHeightConst: NSLayoutConstraint!
    

    var tapGesture = UITapGestureRecognizer()
    
    var trainningTypeArray = [trainningTypeData]()
    var locationDataArray = [locationData]()

    
    var memberCount : Int = 0
    var tempArry = [Int]() // get member is added in the list  or not ---
    
    var trainningOption : String?

    var scrollViewHight : CGFloat = 630
    
    
    //this picker tag is used for the catch current picker view ---
    var pickerCurrentTag : Int = 0

    
    //store the member object --
    var personalInt : Int = 1
    var doubleInt : Int = 1
    var collectiveInt : Int = 1
    
    var dataDict = [String : Any]()
    var trainningObject = [[String : Any]]()
    var currentTag : Int = 12 //for personal view
    var memberObj : AnyObject?
    var memberDict = [Int : [AnyObject]]()
    
    var personalArray = [trainningView]()
    var doubleArray = [trainningView]()
    var collectiveArray = [coollectiveView]()
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        initTapGesture()
        
        //becuase here we have to add default address,
        addMemberBtnPressed(self)
        
        
        //api call for the get master data ---
        
        let queue = DispatchQueue(label: "com.fitit.myqueue" ,qos: .utility)
        queue.async {
           self.getMaterList(masterUrl: locationListApi, tag: 1)
        }
        queue.async {
             self.getMaterList(masterUrl: trainningTypeListApi, tag: -1)
        }
}
    
    //textfiled delegate method ---
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    //get all data from master ---
    @objc func getListData(_ sender: UIButton){

        print(sender.tag)
        pickerCurrentTag = sender.tag
        pickerBackView.isHidden = false
        self.pickerView .reloadAllComponents()
    }
    

    //add tap geture on all img view --
    func initTapGesture(){
        trainningOption = "personal"
    
        for i in 10 ..< 13{
            let view = self.view.viewWithTag(i) as! ExtImg
            if i == 12{
                view.layer .borderWidth = 3.0
                view.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.3607843137, blue: 1, alpha: 1)
            }
            gestureMethod(view: view)
        }
    }
    
    func hanldeAddingView(selectedTag : Int){
        
        if let memberArray = memberDict[selectedTag]{
            
            for i in 0 ..< memberArray.count{
                if selectedTag == 12 || selectedTag == 11{
                    memberObj = memberArray[i] as! trainningView
                }else{
                    memberObj = memberArray[i] as! coollectiveView
                }
                stackView.addArrangedSubview(memberObj as! UIView)
                memberCount += 1
                tempArry.append(i)
            }
        }
        else{
            addMemberBtnPressed(self)
        }
    }
    
    func removeMemberView(selectedTag : Int){
        
        //get the key for find value --
        let keyCollection = memberDict.keys
        let keyArray = Array(keyCollection)
        
        for i in 0 ..< keyArray.count{
            let key = keyArray[i]
            if key != selectedTag{
                if let memberArray = memberDict[key]{
                    
                    for j in 0 ..< memberArray.count{
                        let obj = memberArray[j]
                        obj.removeFromSuperview()
                    }
                }
            }
        }
    }
    
    
    
//init view double and single view ---
    
    func initTrainningView(tag : Int) -> UIView{
        
        //init stack view and items --
        let xib = trainningView()
        
        //add action to the xib button ---
        xib.trainningButton .addTarget(self, action: #selector(getListData(_:)), for: .touchUpInside)
        xib.locationBtn.addTarget(self, action: #selector(getListData(_:)), for: .touchUpInside)
    
        xib.pricePerHourTxt.delegate = self
        xib.trainDurationTxt.delegate = self
        
        xib.frame.size = CGSize(width: stackView.frame.size.width, height: 250.0)
        
        if tag == 12{
            
            xib.trainningButton .tag = tag * -1 * personalInt
            xib.locationBtn .tag = tag * personalInt
            personalInt += personalInt
            personalArray .append(xib)
        }else {
            xib.trainningButton .tag = tag * -1 * doubleInt
            xib.locationBtn .tag = tag * doubleInt
            doubleInt += doubleInt
            doubleArray.append(xib)
        }
        
        if let keyExit = memberDict[tag]{
            var previousArray = keyExit
            previousArray .append(xib)
            memberDict[tag] = previousArray
            
            if tag == 12{
                personalArray.removeAll()
            }else{
                doubleArray.removeAll()
            }
            
        }else{
            //now add the value for upper key that we made --
            if tag == 12{
                memberDict[tag] = personalArray
                personalArray.removeAll()
            }else{
                memberDict[tag] = doubleArray
                doubleArray.removeAll()
            }
        }
        
        //now add to dict
        xib.frame = stackView.bounds
        xib.frame.size = CGSize(width: stackView.frame.size.width, height: 45.0)
        
        return xib
    }
    
    
//init collective view ----
    
    func initCollectiveView(tag : Int) -> UIView{
        
        //init stack view and items --
        let xib = coollectiveView()
        
        xib.trainningButton .tag = tag * -1 * collectiveInt
        xib.locationBtn .tag = tag * collectiveInt
        collectiveInt += collectiveInt

        
        //add action to the xib button ---
        xib.trainningButton .addTarget(self, action: #selector(getListData(_:)), for: .touchUpInside)
        xib.locationBtn.addTarget(self, action: #selector(getListData(_:)), for: .touchUpInside)
        
        xib.participantsTxtFiled.delegate = self
        xib.pricePerHourTxt.delegate = self
        xib.trainDurationTxt.delegate = self
        
        collectiveArray .append(xib)
        
        if let keyExit = memberDict[tag]{
            var previousArray = keyExit
            previousArray .append(xib)
            memberDict[tag] = previousArray
            collectiveArray.removeAll()
            }
        else{
            //now add the value for upper key that we made --
            memberDict[tag] = collectiveArray
            collectiveArray.removeAll()
        }
        
        xib.frame.size = CGSize(width: stackView.frame.size.width, height: 300.0)
        
        //now add to dict
        xib.frame = stackView.bounds
        xib.frame.size = CGSize(width: stackView.frame.size.width, height: 45.0)
        
        return xib
    }
    
    //set data into json object that i have to send on the api
    func setDataObj(){
        
        //get all the address and bind the data --
        let keyCollection = memberDict.keys
        let keyArray = Array(keyCollection)
    
        
        for i in 0 ..< keyArray.count{
            
            //isKeyAdded = false
            let key = keyArray[i]
            let value = memberDict[key] //here we get member that set to the member dict eg-doubel,collection , personal
            
            
            if let count = value?.count{
            
                for i in 0 ..< count{
                
                    dataDict.removeAll()
                    
                    if key == 12{
                        
                        memberObj = value![i] as! trainningView
                        let obj = memberObj as! trainningView
                        
                        let training_id = obj.trainningId
                        let location_id = obj.locationId
                        let training_duration = obj.trainDurationTxt.text
                        let training_wages = obj.pricePerHourTxt.text
                        
                        dataDict["training_option"] = "personal"
                        dataDict["training_typeid"] = training_id
                        dataDict["location_id"] = location_id
                        dataDict["training_duration"] = training_duration
                        dataDict["training_wages"] = training_wages
                        dataDict["participants"] = ""
                        
                        
                    }else if key == 11{
                        
                        memberObj = value![i] as! trainningView
                        let obj = memberObj as! trainningView
                        
                        let training_id = obj.trainningId
                        let location_id = obj.locationId
                        let training_duration = obj.trainDurationTxt.text
                        let training_wages = obj.pricePerHourTxt.text
                        
                        dataDict["training_option"] = "double"
                        dataDict["training_typeid"] = training_id
                        dataDict["location_id"] = location_id
                        dataDict["training_duration"] = training_duration
                        dataDict["training_wages"] = training_wages
                        dataDict["participants"] = ""
                        
                        
                    }else{
                        
                        memberObj = value![i] as! coollectiveView
                        let obj = memberObj as! coollectiveView
                        
                        let training_id = obj.trainningId
                        let location_id = obj.locationId
                        let training_duration = obj.trainDurationTxt.text
                        let training_wages = obj.pricePerHourTxt.text
                        let participents = obj.participantsTxtFiled.text
                        
                        dataDict["training_option"] = "collective"
                        dataDict["training_typeid"] = training_id
                        dataDict["location_id"] = location_id
                        dataDict["training_duration"] = training_duration
                        dataDict["training_wages"] = training_wages
                        dataDict["participants"] = participents
                
                    }
                }
                
                
                //convert all the address dict to json object ---
                if dataDict.count > 0{
                    do{
                        let data =  try JSONSerialization .data(withJSONObject: dataDict, options: [])
                        do{
                            let trainningStr = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                            trainningObject.append(trainningStr as! [String : Any])
                        }catch { }
                        
                    }catch { }
                }
            }
            else {
                
            }
        }
        
        print(trainningObject)
        //if there is any object , then api is called --
        if trainningObject.count > 0 {
            saveUserData()
        }else{
            let msg = "Before proceeding select any option or fill the range of distance."
            alertClass.alertInstance.showAlertWithMessage(Message: msg, ControllerName: self)
        }
    }
    
    
    
    //PASS TO NEXT CONTROLLER --
    func passToNextController(batchId : String){
        let passingController = self.storyboard?.instantiateViewController(withIdentifier: "trnrProfileFifthController") as! trnrProfileFifthController
        self.present(passingController, animated: true, completion: nil)
    }
    
    
    
    //SKIP TO MAIN CONTROLLER --
    func skipToContoller(){
        let passingController = self.storyboard?.instantiateViewController(withIdentifier: "trainerNav") as! UINavigationController
        self.present(passingController, animated: true, completion: nil)
    }
    
}








//MARK: - ALL BUTTON ACTION ---
extension trnrProfileFourController{
    
    
    @IBAction func addMemberBtnPressed(_ sender: Any) {
        memberCount += 1
        
        for i in 0 ..< memberCount{
            
            //add view to the tempArray for check already any view is available or not
            if tempArry .contains(i){
                continue
            }else{
                tempArry.append(i)
                if currentTag == 12{
                    let persnolView = initTrainningView(tag: 12)
                    stackView.addArrangedSubview(persnolView)
                }else if currentTag == 11{
                    let doubleView = initTrainningView(tag: 11)
                    stackView.addArrangedSubview(doubleView)
                }else{
                    let collectionView = initCollectiveView(tag: 10)
                    stackView.addArrangedSubview(collectionView)
                }
                
                //adjust the height of stack view ----

                let stkHeight = CGFloat(250 * (i+1))
                let scrollHeight = scrollViewHight + CGFloat(250 * (i+1))
                stackHeightConst .constant = stkHeight
                mainViewHeightConst.constant = scrollHeight
            }
        }
    }
    
    @IBAction func saveBtnPressed(_ sender: Any) {
        
    }

    
    @IBAction func doneBtnPresesd(_ sender: Any) {
        self.pickerBackView .isHidden = true
    }
    
    
    @IBAction func skipBtnPressed(_ sender: Any) {
        skipToContoller()
    }
    
    
    @IBAction func nextBtnPressed(_ sender: Any) {
        
        let keyCollection = memberDict.keys
        let keyArray = Array(keyCollection)
        
        if keyArray.count > 0{
            setDataObj()
        }
        else{
            let msg = "Please select any trainning option."
            alertClass.alertInstance.showAlertWithMessage(Message: msg, ControllerName: self)
        }
    }
    
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}








//MARK: - ALL API HANDLE ---
extension trnrProfileFourController{
    

    //get location list and trainning type list ---
    
    func getMaterList(masterUrl : String , tag : Int){
        
        SVProgressHUD.show(withStatus: "Please wait...")
        
        var param = [String : Any]()
        param["user_id"] = UserDefaults.standard .string(forKey: userId)
        param["user_access_token"] = UserDefaults.standard .string(forKey: userToken)
        
        apiManager.sharedInstance.handleApiCall(controllerName: self, apiUrl: masterUrl, parameter: param, requestType: httpmethod.post) { (responseJSON) in
            
            if responseJSON["status"] == "success"{
                
                if tag > 0{
                    self.locationDataArray.removeAll()
                    let dataArray = responseJSON["data"].arrayValue
                    
                    for i in 0 ..< dataArray.count{
                        let id = dataArray[i]["location_id"].stringValue
                        let name = dataArray[i]["location_name"].stringValue
                        let locations = locationData(location_id: id, location_name: name)
                        self.locationDataArray.append(locations)
                    }
                }else{
                    
                    self.trainningTypeArray.removeAll()
                    let trainerArray = responseJSON["data"].arrayValue
                    
                    for i in 0 ..< trainerArray.count{
                        let id = trainerArray[i]["training_id"].stringValue
                        let name = trainerArray[i]["training_type"].stringValue
                        let img = trainerArray[i]["training_icon"].stringValue
                       
                        let trinneTypes = trainningTypeData(training_id: id, training_type: name, training_icon: img)
                        self.trainningTypeArray.append(trinneTypes)
                    }
                }
            }
            SVProgressHUD .dismiss()
        }
    }
    
    func saveUserData(){
        
        
        SVProgressHUD .show(withStatus: "Please wait...")
        
        var param = [String : Any]()
        param["user_id"] = UserDefaults.standard.string(forKey: userId)
        param["user_access_token"] = UserDefaults.standard.string(forKey: userToken)
        
        //convert array object to the json string ----
        do{
            let data =  try JSONSerialization .data(withJSONObject: trainningObject, options: [])
            let trainningStr = String.init(data: data, encoding: String.Encoding.utf8)
            param["training_list"] = trainningStr
           // print(trainningStr)
        } catch { }

        
        apiManager.sharedInstance.handleApiCall(controllerName: self, apiUrl: trainerSignUp_3_Api, parameter: param, requestType: httpmethod.post) { (apiData) in
            
            if apiData["status"].stringValue == "success"{
                SVProgressHUD .showSuccess(withStatus: apiData["message"].stringValue)
                
                DispatchQueue.main.async {
                    self.passToNextController(batchId: apiData["data"]["batch_id"].stringValue)
                }
            }
            else{
                SVProgressHUD .showError(withStatus: apiData["message"].stringValue)
            }
        }
    }
}








//MARK: - PICKER VIEW HANDLE ---
extension trnrProfileFourController : UIPickerViewDelegate , UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if  pickerCurrentTag > 0 && locationDataArray .count > 0{
            return locationDataArray.count
        }else if pickerCurrentTag < 0 &&  trainningTypeArray .count > 0{
            return trainningTypeArray.count
        }
        else{
            return 0
        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerCurrentTag > 0{
            let locationType = locationDataArray[row]
            return locationType.location_name
        }
        else{
            let trinningType = trainningTypeArray[row]
            return trinningType.training_type
        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerCurrentTag > 0{
            
            let locationType = locationDataArray[row]
            
            let tag = pickerCurrentTag
            let memberArray = memberDict[tag]
            
           
            if tag == 12{
                
                let indexObj = tag / 12
                memberObj = memberArray![indexObj - 1] as! trainningView
                
                let obj = memberObj as! trainningView
                obj.locationId = locationType.location_id
                obj.locationLbl .text = locationType.location_name
                
            }else if tag == 11{
                
                let indexObj = tag / 11
                memberObj = memberArray![indexObj - 1] as! trainningView
                
                let obj = memberObj as! trainningView
                obj.locationId = locationType.location_id
                obj.locationLbl .text = locationType.location_name
                
            }else{
                
                let indexObj = tag / 10
                memberObj = memberArray![indexObj - 1] as! coollectiveView
                
                let obj = memberObj as! coollectiveView
                obj.locationId = locationType.location_id
                obj.locationLbl .text = locationType.location_name
                
            }

        }
        else{
             let tag = pickerCurrentTag * -1
             let memberArray = memberDict[tag]
            
             let trainningType = trainningTypeArray[row]
            
            
            if tag == 12{
                
                let indexObj = tag / 12
                memberObj = memberArray![indexObj - 1] as! trainningView
                
                let obj = memberObj as! trainningView
                obj.trainningId = trainningType.training_id
                obj.trainningTypeLbl .text = trainningType.training_type
                
            }else if tag == 11{
                
                let indexObj = tag / 11
                memberObj = memberArray![indexObj - 1] as! trainningView
                
                let obj = memberObj as! trainningView
                obj.trainningId = trainningType.training_id
                obj.trainningTypeLbl .text = trainningType.training_type
                
            }else{
                
                let indexObj = tag / 10
                 memberObj = memberArray![indexObj - 1] as! coollectiveView
                
                let obj = memberObj as! coollectiveView
                obj.trainningId = trainningType.training_id
                obj.trainningTypeLbl .text = trainningType.training_type
                
            }
        }
    }
}







//MARK: -  TAP GESTURE HANDLE ---
extension trnrProfileFourController{
    
    func gestureMethod(view : UIImageView){
        
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        view.isUserInteractionEnabled = true
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapView))
        view.addGestureRecognizer(tapGesture)
    }
    
    
    @objc func handleTapView(_ sender:UITapGestureRecognizer){
        
        let tag = sender.view?.tag
        
        for i in 10 ..< 13{
            if tag == i{
                let currentImg = self.view.viewWithTag(i) as! ExtImg
                currentImg.layer.borderWidth = 3.0
                currentImg.layer.borderColor =  #colorLiteral(red: 0.4392156863, green: 0.3607843137, blue: 1, alpha: 1)
               
            }else{
                let currentImg = self.view.viewWithTag(i) as! ExtImg
                currentImg.layer.borderWidth = 0.0
                currentImg.layer.borderColor = UIColor.clear.cgColor
            }
        }
        
        memberCount = 0
        tempArry.removeAll()
        
        switch tag {
        case 12:
            currentTag = 12
            trainningOption = "personal"
            removeMemberView(selectedTag: 12)
            hanldeAddingView(selectedTag: 12)
            
            break
        case 11:
            currentTag = 11
            trainningOption = "double"
            removeMemberView(selectedTag: 11)
            hanldeAddingView(selectedTag: 11)
            break
        case 10:
            currentTag = 10
            trainningOption = "collective"
            removeMemberView(selectedTag: 10)
            hanldeAddingView(selectedTag: 10)
            break
        default:
            trainningOption = ""
            break
        }
    }
}


