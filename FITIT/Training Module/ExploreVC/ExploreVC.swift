//
//  ExploreVC.swift
//  FITIT
//
//  Created by lokesh chand on 31/08/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit

class ExploreVC: UIViewController,UITableViewDataSource,UITableViewDelegate{

    @IBOutlet weak var superview: UIView!
    @IBOutlet var trainerView: UIView!
    @IBOutlet var mapView: UIView!
    @IBOutlet var filterview: UIView!
    
    @IBOutlet var trnrSlider:UILabel!
    @IBOutlet var mapSlider:UILabel!
    
    var selectedTab : Int = 11
    
    @IBOutlet weak var exploretbleView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        superview.addSubview(trainerView)
        trainerView.frame = superview.bounds
        
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        
        //hanlde first time selected tab --
        hanldeSeletedTab(selectedImg: trnrSlider, deselectedImg: mapSlider)
    }
    
    
    @IBAction func tabButtonPressed(_ sender: UIButton)
    {
        
        switch sender.tag
        {
        case 10:
            selectedTab = 10
            hanldeSeletedTab(selectedImg: mapSlider, deselectedImg: trnrSlider)
            trainerView.removeFromSuperview()
            superview.addSubview(mapView)
            mapView.frame = superview.bounds
        case 11:
            selectedTab = 11
            hanldeSeletedTab(selectedImg: trnrSlider, deselectedImg: mapSlider)
            mapView.removeFromSuperview()
            superview.addSubview(trainerView)
            trainerView.frame = superview.bounds
        default:
            break
        }
    }
    

    //MARK:- TablView Methoe--------------------------------------
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        switch selectedTab
        {
        case 11:
            return 12
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 118
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 2
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = Bundle.main.loadNibNamed("ExploretbleCell", owner: self, options: nil)?.first as! ExploretbleCell
        
        switch  selectedTab
        {
        case 11:
            let cell = Bundle.main.loadNibNamed("ExploretbleCell", owner: self, options: nil)?.first as! ExploretbleCell
            return cell
        default:
            return cell
        }
    }
    
    
    @IBAction func filterButtonClick(_ sender: UIButton){
        filterview.isHidden = false
        superview.addSubview(filterview)
    }
    
    @IBAction func filtercrossbuttonClick(_ sender: UIButton){
        filterview.isHidden = true
        
    }
    
    
}

//MARK: - All Custom Function --
extension ExploreVC
{
    
    func hanldeSeletedTab(selectedImg:UILabel , deselectedImg:UILabel)
    {
        selectedImg.isHidden = false
        deselectedImg.isHidden = true
        
    }
}





