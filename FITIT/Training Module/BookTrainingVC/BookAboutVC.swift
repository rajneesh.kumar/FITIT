//
//  BookAboutVC.swift
//  FITIT
//
//  Created by lokesh chand on 04/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//
import UIKit

class BookAboutVC: UIViewController,UIScrollViewDelegate {
    
    @IBOutlet weak var pageControl:UIPageControl!
    @IBOutlet weak var scrollView:UIScrollView!
    @IBOutlet var windowSlider:UILabel!
    @IBOutlet var traininigSlider:UILabel!
    @IBOutlet var aboutSlider:UILabel!
    @IBOutlet weak var superview: UIView!
    @IBOutlet var windowView: UIView!
    @IBOutlet var traningView: UIView!
    @IBOutlet var aboutView: UIView!
    
    var selectedTab : Int = 12

    var images:[String] = ["11","22","33"]
    var frame = CGRect(x: 0, y: 0, width: 0, height: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("git coommand fir se")
        print("Git commnaf from rajneesh")
        print("git command from lokesh")
        
        superview.addSubview(aboutView)
        aboutView.frame = superview.bounds
        
        
        pageControl.numberOfPages = images.count
        for index in 0..<images.count{
            
            frame.origin.x = scrollView.frame.size.width * CGFloat(index)
            frame.size = scrollView.frame.size
            let imgView = UIImageView(frame: frame)
            imgView.image = UIImage(named: images[index])
            self.scrollView.addSubview(imgView)
            view.bringSubview(toFront: pageControl)
            
            
        }
        
        scrollView.contentSize = CGSize(width: (scrollView.frame.size.width * CGFloat(images.count)), height: scrollView.frame.size.height)
        scrollView.delegate = self
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        
        //hanlde first time selected tab --
        hanldeSeletedTab(selectedImg: aboutSlider, deselectedImg: windowSlider, deselectedImg2: traininigSlider)
    }
    
    
    @IBAction func tabButtonPressed(_ sender: UIButton)
    {
        
        switch sender.tag
        {
        case 10:
            selectedTab = 10
          hanldeSeletedTab(selectedImg: windowSlider, deselectedImg: traininigSlider, deselectedImg2: aboutSlider)
            aboutView.removeFromSuperview()
            traningView.removeFromSuperview()
            superview.addSubview(windowView)
            windowView.frame = superview.bounds
        case 11:
            selectedTab = 11
            hanldeSeletedTab(selectedImg: traininigSlider, deselectedImg: windowSlider, deselectedImg2: aboutSlider)
            aboutView.removeFromSuperview()
            windowView.removeFromSuperview()
            superview.addSubview(traningView)
            traningView.frame = superview.bounds
        case 12:
            selectedTab = 12
            hanldeSeletedTab(selectedImg: aboutSlider, deselectedImg: windowSlider, deselectedImg2: traininigSlider)
             windowView.removeFromSuperview()
             traningView.removeFromSuperview()
            superview.addSubview(aboutView)
            aboutView.frame = superview.bounds
        default:
            break
        }
    }
    
    //MARK:- ScrollView Method
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = scrollView.contentOffset.x / scrollView.frame.size.width
        pageControl.currentPage = Int(pageNumber)
    }
    
}

//MARK: - All Custom Function --
extension BookAboutVC
{
    
    func hanldeSeletedTab(selectedImg:UILabel, deselectedImg:UILabel, deselectedImg2:UILabel)
    {
        selectedImg.isHidden = false
        deselectedImg.isHidden = true
        deselectedImg2.isHidden = true
        
    }
}

extension BookAboutVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        switch selectedTab
        {
        case 10:
            return 115
        case 11:
            return 76
            
        default:
            return 0
        }

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch selectedTab
        {
        case 11:
            return 3
        case 10:
            return 5
        default:
            return 0
        }

       }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
         let cell = Bundle.main.loadNibNamed("BooKTrainingCell", owner: self, options: nil)?.first as! BooKTrainingCell

        switch  selectedTab
        {
        case 10:
           let cell = tableView.dequeueReusableCell(withIdentifier: "BooKWindowCell", for: indexPath) as! BooKWindowCell
           return cell
        case 11:
             let cell = Bundle.main.loadNibNamed("BooKTrainingCell", owner: self, options: nil)?.first as! BooKTrainingCell
            return cell
        default:
             return cell
        }
    }
    
}





