//
//  BookLocationVC.swift
//  FITIT
//
//  Created by lokesh chand on 05/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit

class BookLocationVC: UIViewController,UITableViewDataSource,UITableViewDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 87
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BookLocationCell", for: indexPath) as! BookLocationCell
        
        return cell
    }
 

}
