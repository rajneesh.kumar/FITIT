//
//  BookDateVC.swift
//  FITIT
//
//  Created by lokesh chand on 05/09/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit

class BookDateVC: UIViewController {
@IBOutlet weak var myCollectionview: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}

extension BookDateVC:UICollectionViewDelegate,UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 16
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView .dequeueReusableCell(withReuseIdentifier: "BookDateCollectionCell", for: indexPath) as! BookDateCollectionCell
        
        return cell
    }
    
    
    
}


extension BookDateVC:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let collectionWidth = myCollectionview.bounds.width
        return CGSize(width: collectionWidth/4 - 16, height: collectionWidth/4 - 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    
}
