//
//  CreateOneCollectionCell.swift
//  FITIT
//
//  Created by lokesh chand on 30/08/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit

class CreateOneCollectionCell: UICollectionViewCell {
    
    
    @IBOutlet weak var bgView: ExtView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var intrestLbl: UILabel!
}
