//
//  CreateProfileVC.swift
//  FITIT
//
//  Created by lokesh chand on 30/08/18.
//  Copyright © 2018 invetech. All rights reserved.
//


import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class CreateProfileVC: UIViewController, UITextFieldDelegate{

    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var phoneTxtFiled: textFieldExt!
    @IBOutlet weak var firstTxtFiled: textFieldExt!
    @IBOutlet weak var lastNameTxtFiled: textFieldExt!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var dateView: ExtView!
    @IBOutlet weak var pickerBgView: UIView!
    
    @IBOutlet weak var segmnetController: UISegmentedControl!
    
    var seletedDate = ""
    var userGender = "f"
    var userImg : UIImage?
    var selectedDate = ""
    
    var imagePicker = UIImagePickerController()
    var tapGesture = UITapGestureRecognizer()
    
     var keyboardToolbar: UIToolbar?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addTapGesture(view: dateView)
        addImgTapGesture(view: profileImgView)
        
        dynamicToobarHandle()
    }
    
//add toolbar to keyboard --
    func dynamicToobarHandle()
    {
        if keyboardToolbar == nil
        {
            keyboardToolbar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(view.bounds.size.width), height: CGFloat(35)))
            
            let extraspace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            
            let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(resignKeyboard))
            
            keyboardToolbar?.items = [extraspace, doneButton]
            
            keyboardToolbar?.barStyle = .default
        }
        phoneTxtFiled.inputAccessoryView = keyboardToolbar
    }
 
//resgin keyboard ---
    @objc func resignKeyboard()
    {
        phoneTxtFiled.resignFirstResponder()
    }
    
//textfiled Delegate and method ---
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
//PASS TO DIRECT DASHBOARD--
    func passToDashboard(){
        
        //let trainningStoryboard = UIStoryboard(name: "Trainning", bundle: nil)
        let passingController = self.storyboard?.instantiateViewController(withIdentifier: "trainningNav") as! UINavigationController
        self.present(passingController, animated: true, completion: nil)
    }
    
//PASS TO NEXT CONTROLLER --
    func passToNextController(){
        let passingController = self.storyboard?.instantiateViewController(withIdentifier: "CreateOneVC") as! CreateOneVC
        self.present(passingController, animated: true, completion: nil)
    }
    

//ACTION METHOD FOR THE  DATE PICKER --
    @IBAction func dateValueChanged(_ sender: UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        selectedDate = dateFormatter.string(from: sender.date)
        dateLbl .text = selectedDate
    }
}








//MARK: - ALL BUTTON ACTION HERE ---
extension CreateProfileVC{
    
    @IBAction func genderBtnPressed(_ sender: UISegmentedControl) {
        if segmnetController.selectedSegmentIndex == 0{
            userGender = "f"
        }else{
            userGender = "m"
        }
    }
    
    @IBAction func nextBtnPressed(_ sender: Any) {
        
        if (firstTxtFiled .text?.count)! > 0{
            if (lastNameTxtFiled .text?.count)! > 0{
                if selectedDate != ""{
                    if (phoneTxtFiled.text?.count)! > 0{
                        if userImg != nil{
                            
                            handleUserData()  //call the api
                            
                        }else{
                            let msg = "Please select your profile photo."
                            alertClass.alertInstance.showAlertWithMessage(Message: msg, ControllerName: self)
                        }
                    }else{
                        let msg = "Please enter your mobile number."
                        alertClass.alertInstance.showAlertWithMessage(Message: msg, ControllerName: self)
                    }
                }else{
                    let msg = "Please select your date of birth"
                    alertClass.alertInstance.showAlertWithMessage(Message: msg, ControllerName: self)
                }
            }else{
                let msg = "Please enter your last name."
                alertClass.alertInstance.showAlertWithMessage(Message: msg, ControllerName: self)
            }
        }else{
            let msg = "Please enter your first name."
            alertClass.alertInstance.showAlertWithMessage(Message: msg, ControllerName: self)
        }
    }
    
    
    @IBAction func skipBtnPressed(_ sender: Any) {
        passToDashboard()
    }
    
    
    @IBAction func doneBtnPressed(_ sender: Any) {
        self.pickerBgView .isHidden = true
    }

}





//MARK: - HANDLE USER DATA ---
extension CreateProfileVC{
    
    func handleUserData(){
        
        SVProgressHUD.show(withStatus: "Please wait...")
        var param = [String : Any]()
        param["user_id"] = UserDefaults.standard .string(forKey: userId)
        param["user_access_token"] = UserDefaults.standard .string(forKey: userToken)
        param["user_fname"] = firstTxtFiled.text
        param["user_lname"] = lastNameTxtFiled.text
        param["user_dob"] = dateLbl.text
        param["user_phone"] = phoneTxtFiled.text
        param["user_gender"] = userGender
        
        let headers: HTTPHeaders = [ "Content-type": "multipart/form-data" ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in param{
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            //set the user image ---
            if let imgData = UIImageJPEGRepresentation(self.userImg!, 0.6){
                multipartFormData.append(imgData, withName: "user_profile_pic", fileName: "userProfile.jpg", mimeType: "image")
            }
            
        }, usingThreshold: UInt64.init(), to: traineeSignUpApi, method: .post, headers: headers) { (result) in
            
            switch result
            {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    
                   print(JSON(response.data))
                    SVProgressHUD .dismiss()
                    DispatchQueue.main.async {
                        self.passToNextController()
                    }
                   
                    if let err = response.error{
                        print(err)
                        return
                    }
                }
                
            case .failure(let error):
                 print("Error in upload: \(error.localizedDescription)")
                SVProgressHUD.dismiss()
            }
        }
    }
}

//MARK: - HANDLE IMAGE SOURCE ---
extension CreateProfileVC : UIImagePickerControllerDelegate , UINavigationControllerDelegate{
    
    func handleImageSelection(){
        self.imagePicker.sourceType = .photoLibrary
        self.imagePicker.delegate = self
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]){
        
        userImg = info["UIImagePickerControllerOriginalImage"] as? UIImage
        profileImgView .image = userImg
        self.dismiss(animated: true, completion: nil)
    }
    

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        self.dismiss(animated: true, completion: nil)
    }
}


//MARK: - HANDLE TAP GESTURE ---
extension CreateProfileVC{
    
    
//THIS TAP GESTURE ONLY FOR THE DATE VIEW
    func addTapGesture(view : UIView)
    {
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        view.isUserInteractionEnabled = true
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapView(_:)))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func handleTapView(_ sender:UITapGestureRecognizer){
         self.pickerBgView .isHidden = false
         view.endEditing(true)
    }
    
    
    
 //THIS TAP GESTURE ONLY FOR THE DATE VIEW
    func addImgTapGesture(view : UIImageView)
    {
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        view.isUserInteractionEnabled = true
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleImgTapView(_:)))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func handleImgTapView(_ sender:UITapGestureRecognizer){
        self.handleImageSelection()
    }
}
