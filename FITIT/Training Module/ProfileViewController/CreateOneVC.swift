//
//  CreateOneVC.swift
//  FITIT
//
//  Created by lokesh chand on 30/08/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON
import Alamofire
import SDWebImage



//MARK: - USER INTREST MODEL --
struct intrestData {
    
    var intrestImageUrl : String?
    var intrestid : Int?
    var intrestType : String?
}





class CreateOneVC: UIViewController {
    
    @IBOutlet weak var myCollectionview: UICollectionView!
    
    var selectedData = [[String: String]]()
    var selectedItemArray = [Int]()
    var someDict = [String : Any]()
    
    var intrestDataArry = [intrestData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        getInterstList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        selectedData.removeAll()
        selectedItemArray.removeAll()
    }
    
    
    //MARK: - ALL CUSTEM METHOD --
    func passToNext(){
        
        let passingController = self.storyboard?.instantiateViewController(withIdentifier: "CreateTwoVC") as! CreateTwoVC
        self.present(passingController, animated: true, completion: nil)
    }
    
    //PASS TO DIRECT DASHBOARD--
    func passToDashboard(){
        
        //let trainningStoryboard = UIStoryboard(name: "Trainning", bundle: nil)
        let passingController = self.storyboard?.instantiateViewController(withIdentifier: "trainningNav") as! UINavigationController
        self.present(passingController, animated: true, completion: nil)
    }
    
    
}


//MARK: - ALL ACTION AND METHOD --
extension CreateOneVC{
    
    @IBAction func skipPressed(_ sender: Any) {
        passToDashboard()
    }
    
    @IBAction func nextBtnPressed(_ sender: Any) {
        
        if selectedItemArray.count > 0{
            for i in 0..<selectedItemArray.count {
                
                let dict = ["id": "\(selectedItemArray[i])"]
                selectedData.append(dict)
            }
            saveUserData()
        }else{
            alertClass.alertInstance.showAlertWithMessage(Message: "Please select your intrest.", ControllerName: self)
        }
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}


//MARK: - API HANDLE --
extension CreateOneVC{
    
    
    //get interst list of user ---
    func getInterstList(){
        
        SVProgressHUD .show(withStatus: "Please wait...")
        
        var param = [String : Any]()
        param["user_id"] = UserDefaults.standard .string(forKey: userId)
        param["user_access_token"] = UserDefaults.standard .string(forKey: userToken)
        
        apiManager.sharedInstance.handleApiCall(controllerName: self, apiUrl: getIntrestListApi, parameter: param, requestType: httpmethod.post) { (responseData) in
            
            if responseData["status"].string == "success"{
                
                let apiDataArray = responseData["data"].arrayValue
                
                for i in 0 ..< apiDataArray.count{
                    
                    let instId = apiDataArray[i]["training_id"].intValue
                    let instType = apiDataArray[i]["training_type"].stringValue
                    let instUrl = apiDataArray[i]["training_icon"].stringValue
                
                    let userIntrest = intrestData(intrestImageUrl: instUrl, intrestid: instId, intrestType: instType)
                    
                    self.intrestDataArry.append(userIntrest)
                }
                    DispatchQueue.main.async {
                        self.myCollectionview.reloadData()
                        SVProgressHUD.dismiss()
                }
            }else{
                SVProgressHUD.dismiss()
            }
        }
    }
    
    
    //save user data while signup step--
    func saveUserData(){
        
        SVProgressHUD .show(withStatus: "Please wait...")
        
        //convert array object to the json string ----
        do{
            let data =  try JSONSerialization .data(withJSONObject: selectedData, options: [])
            let intrestIdStr = String.init(data: data, encoding: String.Encoding.utf8)
            

            someDict["user_id"] = UserDefaults.standard .string(forKey: userId)
            someDict["user_access_token"] = UserDefaults.standard .string(forKey: userToken)
            someDict["interest_ids"] = intrestIdStr
        }
            
        catch{ print("some error to parse") }
        
        //api url for the hit -----
        let url = URL(string: trineeSignUP_2_Api)

        request(url!, method: .post, parameters: someDict, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            
            switch response.result{
                
            case .success(let datas ):
                let responseData = JSON(datas)
                if responseData["status"].stringValue == "success"{
                    DispatchQueue.main.async {
                        self.passToNext()
                    }
                    SVProgressHUD.showSuccess(withStatus: responseData["message"].stringValue)
                }else{
                    SVProgressHUD.showError(withStatus: responseData["message"].stringValue)
                }
                
            case .failure(let error):
                print(error.localizedDescription)
                 SVProgressHUD.showError(withStatus: "Please try again.")
            }
        }
    }
}


//MARK: - COLLECTION DELEGATE AND METHOD --
extension CreateOneVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return intrestDataArry.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        let cell = collectionView .dequeueReusableCell(withReuseIdentifier: "CreateOneCollectionCell", for: indexPath) as! CreateOneCollectionCell
       
        //set the data --
        let userIntrest = intrestDataArry[indexPath.item]
        
        if let selectedId = userIntrest.intrestid{
            
            if selectedItemArray .contains(selectedId){
                cell.intrestLbl .textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                cell.bgView .backgroundColor = #colorLiteral(red: 0.4392156863, green: 0.3607843137, blue: 1, alpha: 1)
            }
            else{
                cell.intrestLbl .textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                cell.bgView .backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }
        }
        
        cell.intrestLbl .text = userIntrest.intrestType
        
        //get the image --
        if let intrestImgStr =  userIntrest.intrestImageUrl{
            let imgUrl = URL(string:intrestImgStr)
            cell.imgView.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "placeholder.png"))
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let userIntrest = intrestDataArry[indexPath.item]
        
        if let selectedId = userIntrest.intrestid{
            
            if selectedItemArray .contains(selectedId){
                let index = selectedItemArray.index(of: selectedId)
                selectedItemArray.remove(at: index!)
            }else{
                selectedItemArray .append(selectedId)
            }
            self.myCollectionview.reloadData()
        }
    }
    

//collection size class method ---
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        let cellSize = myCollectionview.bounds.width / 3  - 15
        return CGSize(width: cellSize, height: cellSize)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
}

