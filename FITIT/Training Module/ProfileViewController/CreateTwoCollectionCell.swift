//
//  CreateTwoCollectionCell.swift
//  FITIT
//
//  Created by lokesh chand on 31/08/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit

class CreateTwoCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var backview: ExtView!
    @IBOutlet weak var goalLbl: UILabel!
    @IBOutlet weak var imgView: UIImageView!
}
