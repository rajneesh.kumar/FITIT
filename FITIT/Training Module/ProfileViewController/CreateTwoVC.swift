//
//  CreateTwoVC.swift
//  FITIT
//
//  Created by lokesh chand on 31/08/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON
import Alamofire



//MARK: - GOAL MODEL --
struct goalData {
    
    var goalImageUrl : String?
    var goalid : Int?
    var goalType : String?
}





class CreateTwoVC: UIViewController {
    
    @IBOutlet weak var myCollectionview: UICollectionView!
    @IBOutlet weak var fatTxtFiled: textFieldExt!
    @IBOutlet weak var heightTxtFiled: textFieldExt!
    @IBOutlet weak var weightTxtFiled: textFieldExt!
    @IBOutlet weak var loweView: UIView!
    
    
    var selectedData = [[String: String]]()
    var selectedItemArray = [Int]()
    
    var someDict = [String : Any]()
    var goalDataArry = [goalData]()
    
    var keyboardToolbar: UIToolbar?
    let userType = UserDefaults.standard .string(forKey: userLoginType)
    

    override func viewDidLoad() {
        super.viewDidLoad()

        getGoalist()
        dynamicToobarHandle()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        selectedItemArray.removeAll()
        selectedData.removeAll()
    }
    
    
    //add toolbar to keyboard --
    func dynamicToobarHandle()
    {
        if keyboardToolbar == nil
        {
            keyboardToolbar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(view.bounds.size.width), height: CGFloat(35)))
            
            let extraspace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            
            let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(resignKeyboard))
            
            keyboardToolbar?.items = [extraspace, doneButton]
            
            keyboardToolbar?.barStyle = .default
        }
        heightTxtFiled.inputAccessoryView = keyboardToolbar
        weightTxtFiled.inputAccessoryView = keyboardToolbar
        fatTxtFiled.inputAccessoryView = keyboardToolbar
    }
    
    
    
    //resgin keyboard ---
    @objc func resignKeyboard(){
         heightTxtFiled.resignFirstResponder()
         weightTxtFiled.resignFirstResponder()
         fatTxtFiled.resignFirstResponder()
    }
    
    
 //MARK: - pass to trainning controller --
    
    func passToTrainningController(){
    
    //let trainningStoryboard = UIStoryboard(name: "Trainning", bundle: nil)
        let passingController = self.storyboard?.instantiateViewController(withIdentifier: "trainningNav") as! UINavigationController
    self.present(passingController, animated: true, completion: nil)
    }
    
   
    func passToNext(){
  
        if userType == "trainee"{
            passToTrainningController()
        }else{
            print("do nothing")
        }
    }
    
 }

//MARK: - ALL ACTION AND METHOD --
extension CreateTwoVC{
    
    @IBAction func skipPressed(_ sender: Any) {
        
        if userType == "trainee"{
            passToTrainningController()
        }else{
            print("do nothing")
        }
    }
    
    @IBAction func nextBtnPressed(_ sender: Any) {
        
        if selectedItemArray.count > 0{
            for i in 0..<selectedItemArray.count {
                
                let dict = ["id": "\(selectedItemArray[i])"]
                selectedData.append(dict)
            }
            saveUserData()
        }else{
            alertClass.alertInstance.showAlertWithMessage(Message: "Please select your goal.", ControllerName: self)
        }
    }
    
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}



//MARK: - API HANDLE --
extension CreateTwoVC{
    
    
    //get interst list of user ---
    func getGoalist(){
        
        SVProgressHUD .show(withStatus: "Please wait...")
        
        var param = [String : Any]()
        param["user_id"] = UserDefaults.standard .string(forKey: userId)
        param["user_access_token"] = UserDefaults.standard .string(forKey: userToken)
        
        apiManager.sharedInstance.handleApiCall(controllerName: self, apiUrl: getGoalListApi, parameter: param, requestType: httpmethod.post) { (responseData) in
            
            if responseData["status"].string == "success"{
                
                let apiDataArray = responseData["data"].arrayValue
                
                for i in 0 ..< apiDataArray.count{
                    
                    let goalId = apiDataArray[i]["trng_goal_id"].intValue
                    let goalType = apiDataArray[i]["trng_goal_name"].stringValue
                    let goalUrl = apiDataArray[i]["trng_goal_icon"].stringValue
                    
                    let userGoal = goalData(goalImageUrl: goalUrl, goalid: goalId, goalType: goalType)
                    
                    self.goalDataArry.append(userGoal)
                }
                    DispatchQueue.main.async {
                        self.myCollectionview.reloadData()
                        SVProgressHUD.dismiss()
                }
            }else{
                SVProgressHUD.dismiss()
            }
        }
    }
    
    
    
    func saveUserData(){
        
        SVProgressHUD .show(withStatus: "Please wait...")
        
        //convert array object to the json string ----
        do{
            let data =  try JSONSerialization .data(withJSONObject: selectedData, options: [])
            let goalIdStr = String.init(data: data, encoding: String.Encoding.utf8)
            
            
            someDict["user_id"] = UserDefaults.standard .string(forKey: userId)
            someDict["user_access_token"] = UserDefaults.standard .string(forKey: userToken)
            someDict["goal_ids"] = goalIdStr
            someDict["fat"] = ""
            someDict["height"] = ""
            someDict["weight"] = ""
            
        }
            
        catch{ print("some error to parse") }
        
        //api url for the hit -----
        let url = URL(string: traineeSignUp_3_Api)
        
        request(url!, method: .post, parameters: someDict, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            
            switch response.result{
                
            case .success(let datas ):
                
                let responseData = JSON(datas)
                if responseData["status"].stringValue == "success"{
                    DispatchQueue.main.async {
                        self.passToNext()
                    }
                    SVProgressHUD.showSuccess(withStatus: responseData["message"].stringValue)
                }else{
                    SVProgressHUD.showError(withStatus: responseData["message"].stringValue)
                }
                
                
            case .failure(let error):
                print(error.localizedDescription)
                SVProgressHUD.showError(withStatus: "Please try again.")
            }
        }
    }
}



//MARK: - COLLECTION DELEGATE AND DATASOURCE--

extension CreateTwoVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return goalDataArry.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView .dequeueReusableCell(withReuseIdentifier: "CreateTwoCollectionCell", for: indexPath) as! CreateTwoCollectionCell
        
        //set the data --
        let userGoal = goalDataArry[indexPath.item]
        
        if let selectedId = userGoal.goalid{
            
            if selectedItemArray .contains(selectedId){
                cell.goalLbl .textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                cell.backview .backgroundColor = #colorLiteral(red: 0.4392156863, green: 0.3607843137, blue: 1, alpha: 1)
            }
            else{
                cell.goalLbl .textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                cell.backview .backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }
        }
        
        cell.goalLbl .text = userGoal.goalType
        
        //get the image --
        if let goalImgStr =  userGoal.goalImageUrl{
            let imgUrl = URL(string:goalImgStr)
            cell.imgView.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "placeholder.png"))
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let userGoal = goalDataArry[indexPath.item]
        
        if let selectedId = userGoal.goalid{
            
            if selectedItemArray .contains(selectedId){
                let index = selectedItemArray.index(of: selectedId)
                
                selectedItemArray.remove(at: index!)
            }else{
                selectedItemArray .append(selectedId)
            }
            loweView .isHidden = false
            self.myCollectionview.reloadData()
        }
    }
    //flow layout delegate method ---
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let collectionWidth = myCollectionview.bounds.width
        return CGSize(width: collectionWidth/3 - 16, height: collectionWidth/3 - 16)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
    
}

